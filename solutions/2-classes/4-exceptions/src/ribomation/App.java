package ribomation;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class App {
    public static String FILENAME = "whatever.txt";

    public static void main(String[] args) {
        System.out.println("App.main BEFORE");
        App app = new App();
        app.run();
        System.out.println("App.main AFTER");
    }

    private void run() {
        System.out.println("App.run BEFORE");
        try {
            func1();
        } catch (Exception e) {
            System.out.printf("ooops: %s%n", e);
            e.printStackTrace(System.out);

            var stkTrace = e.getStackTrace();
            var firstElem = stkTrace[0];
            System.out.printf("elem: %s:%d - %s%n",
                    firstElem.getFileName(),
                    firstElem.getLineNumber(),
                    firstElem.getMethodName());
//            for (var ste : stkTrace)
//                System.out.printf("elem: %s:%d - %s%n",
//                        ste.getFileName(), ste.getLineNumber(), ste.getMethodName());
        }
        System.out.println("App.run AFTER");
    }

    private void func1() {
        System.out.println("App.func1 BEFORE");
        func2();
        System.out.println("App.func1 AFTER");
    }

    private void func2() {
        System.out.println("App.func2 BEFORE");
        bizop();
        System.out.println("App.func2 AFTER");
    }

    private void bizop() {
        System.out.println("App.bizop BEFORE");

        try {
            var f = new FileReader(FILENAME);
            char[] buf = new char[100];
            f.read(buf);
            f.close();
        } catch (FileNotFoundException e) {
            throw new AppException("cannot find file: " + FILENAME, e);
        } catch (IOException e) {
            throw new AppException("read fault", e);
        } catch (Exception e) {
            throw new AppException("no idea", e);
        }

//        try {
//            Object obj = null;
//            var s = obj.toString();
//        } catch (NullPointerException e) {
//            throw new AppException(e);
//        }
        System.out.println("App.bizop AFTER");
    }
}
