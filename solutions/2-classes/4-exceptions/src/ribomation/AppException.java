package ribomation;

public class AppException extends RuntimeException {
    public AppException(String reason, Exception ex) {
        super(reason, ex);
    }
}
