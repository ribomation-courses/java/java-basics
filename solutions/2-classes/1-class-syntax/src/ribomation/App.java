package ribomation;

public class App {
    public static void main(String[] args) {
        App app = new App();
        app.run();
    }

    private void run() {
        Car car = new Car("XYZ123", "Volvo Amazon");
        System.out.printf("car=%s%n", car);

        Person person = new Person("Nisse");
        System.out.printf("person=%s%n", person);

        person.setCar(car);
        System.out.printf("person=%s%n", person);
    }
}
