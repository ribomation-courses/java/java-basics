package se.ribomation.app;

public class BizOps {
    void func1() throws AppException {
        try {
            func2();
        } catch (Exception e) {
            throw new AppException(e);
        }
    }

    void func2() {
        func3();
    }

    void func3() {
        Object obj = null;
        System.out.printf("%s%n", obj.toString());
    }
}
