package se.ribomation.app;

import java.io.PrintWriter;
import java.io.StringWriter;

public class App {
    public static void main(String[] args) {
        var app = new App();
        app.run();
    }

    private void run() {
        try {
            var biz = new BizOps();
            biz.func1();
        } catch (AppException e) {
            System.out.printf("Error: %s%n", e);

            var buf = new StringWriter();
            e.printStackTrace(new PrintWriter(buf));
            System.out.printf("Here is the stack trace:%n%s%n", buf);
        }
    }
}
