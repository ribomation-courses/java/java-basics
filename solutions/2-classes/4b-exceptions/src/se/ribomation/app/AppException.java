package se.ribomation.app;

public class AppException extends Exception {
    public AppException(Exception err) {
        super("Ooops a bug!!", err);
    }
}
