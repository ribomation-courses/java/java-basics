package ribomation;

public class Triangle extends Shape {
    private final int base;
    private final int height;

    public Triangle(int base, int height) {
        this.base = base;
        this.height = height;
    }

    @Override
    public float area() {
        return (float) (base * height / 2.0);
    }

}
