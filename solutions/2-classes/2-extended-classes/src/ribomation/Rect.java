package ribomation;

public class Rect extends Shape {
    private final int width;
    private final int height;

    public Rect(int width, int height) {
        this.width = width;
        this.height = height;
    }

    @Override
    public float area() {
        return width * height;
    }
}
