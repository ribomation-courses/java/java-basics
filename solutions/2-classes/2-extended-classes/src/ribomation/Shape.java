package ribomation;

public abstract class Shape {
    public abstract float area();
}
