package se.ribomation.shapes;

public class Square extends Shape {
    private int side;

    public Square(int side) {
        this.side = side;
    }

    @Override
    public float area() {
        return side * side;
    }
}
