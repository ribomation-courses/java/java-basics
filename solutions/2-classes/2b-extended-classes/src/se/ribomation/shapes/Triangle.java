package se.ribomation.shapes;

public class Triangle extends Shape {
    private int width;
    private int height;

    public Triangle(int width, int height) {
        this.width = width;
        this.height = height;
    }

    @Override
    public float area() {
        return (float) (width * height / 2.0);
    }
}
