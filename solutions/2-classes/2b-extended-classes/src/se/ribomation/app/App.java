package se.ribomation.app;

import se.ribomation.shapes.Shape;

public class App {
    public static void main(String[] args) {
        var app = new App();
        app.run();
    }

    private void run() {
        var list = Shape.create(5_000);
        print(list);
    }

    private void print(Shape[] shapes) {
        for (Shape s : shapes) {
            System.out.printf("%s: area = %.2f%n",
                    s.getClass().getSimpleName(),
                    s.area());
        }
    }

}
