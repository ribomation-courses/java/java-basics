package se.ribomation.app;

public class App {
    public static void main(String[] args) {
        var app = new App();
        app.run();
    }

    private void run() {
        var car = new Car("ABC123", "Fiesta");
        System.out.printf("car: %s%n", car);

        var person = new Person("Justin Time");
        System.out.printf("person: %s%n", person);

        person.setCar(car);
        System.out.printf("person: %s%n", person);

        person.setCar(null);
        System.out.printf("person.car: %s%n", person.getCar());
    }
}
