package se.ribomation.products.domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ProductRepoTest {
    private ProductRepo target;

    @BeforeEach
    void setUp() {
        target = new ProductRepo();
    }

    @Test
    @DisplayName("loading three CSV products, should pass")
    void loadCSV() throws IOException {
        var csvFile = "./src/test/resources/few-products.csv";
        var result = target.loadCSV(csvFile);
        assertThat(result, notNullValue());
        assertThat(result.size(), is(3));

        assertThat(result.get(0).getName(), is("apple"));
        assertThat(result.get(1).getName(), is("banana"));
        assertThat(result.get(2).getName(), is("coco nut"));

        assertThat(result.stream().mapToInt(Product::getCount).sum(), is(10));
    }

    @Test
    @DisplayName("loading a non-existing file, should throw ioexception")
    void loadCSV2() {
        assertThrows(IOException.class, () ->
                target.loadCSV("no-such-file.csv"));
    }

    @Test
    @DisplayName("loading null file, should throw nullptr")
    void loadCSV3() {
        assertThrows(NullPointerException.class, () ->
                target.loadCSV(null));
    }

}