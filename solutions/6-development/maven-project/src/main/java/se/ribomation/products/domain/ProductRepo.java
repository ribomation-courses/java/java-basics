package se.ribomation.products.domain;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Factory that can load products from CSV files, or generate fake products.
 */
public class ProductRepo {
    private Random r = new Random();

    /**
     * Loads a CSV file and returns a list of products
     *
     * @param csvFile name of file
     * @param delimiter CSV delimiter to split around
     * @return list of products
     * @throws IOException if the file cannot be found
     */
    public List<Product> loadCSV(String csvFile, String delimiter) throws IOException {
        return Files.lines(Path.of(csvFile))
                .skip(1) //header
                .map(line -> line.split(delimiter))
                .map(fields -> new Product(fields[0], fields[1], fields[2]))
                .collect(Collectors.toList());
    }

    /**
     * Loads a CSV file and returns a list of products
     *
     * @param csvFile name of file
     * @return list of products
     * @throws IOException if the file cannot be found
     */
    public List<Product> loadCSV(String csvFile) throws IOException {
        return loadCSV(csvFile, ";");
    }

        /**
         * Generate a single product
         * @return a product
         */
    public Product create() {
        String name  = names[r.nextInt(names.length)];
        float  price = Math.abs((float) (r.nextGaussian() * 100 + 50));
        int    count = r.nextInt(10);
        return new Product(name, price, count);
    }

    /**
     * Generate a list of products
     * @param n how many to generate
     * @return list of products
     */
    public List<Product> create(int n) {
        var result = new ArrayList<Product>(n);
        for (var k = 0; k < n; ++k) result.add(create());
        return result;
    }

    private static final String[] names = {
            "apple", "banana", "coco nut", "date plum", "kiwi", "orange", "peach"
    };
}
