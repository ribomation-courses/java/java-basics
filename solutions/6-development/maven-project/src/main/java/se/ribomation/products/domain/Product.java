package se.ribomation.products.domain;

import java.util.Objects;

/**
 * Represents a single product
 */
public class Product implements Comparable<Product> {
    private String name;
    private float  price;
    private int    count;

    public Product(String name, String price, String count) {
        this(name, Float.parseFloat(price), Integer.parseInt(count));
    }

    public Product(String name, float price, int count) {
        this.name = name;
        this.price = price;
        this.count = count;
    }

    @Override
    public String toString() {
        return String.format("Product{%s, %.2f kr, %d left}", name, price, count);
    }

    public String getName() {
        return name;
    }

    public float getPrice() {
        return price;
    }

    public int getCount() {
        return count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Float.compare(product.price, price) == 0 &&
                count == product.count &&
                name.equals(product.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, price, count);
    }

    @Override
    public int compareTo(Product o) {
        return this.name.compareTo(o.name);
    }
}
