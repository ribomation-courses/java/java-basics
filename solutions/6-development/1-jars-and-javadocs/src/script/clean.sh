#!/usr/bin/env bash
set -eux

SCRIPT_DIR=`dirname $0`
. "${SCRIPT_DIR}/_settings.sh"

rm -rf ${BUILD_DIR}
