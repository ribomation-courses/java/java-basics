#!/usr/bin/env bash
set -eux

SCRIPT_DIR=`dirname $0`
. "${SCRIPT_DIR}/_settings.sh"

rm -rf ${BUILD_DIR}
mkdir -p ${CLASS_DIR} ${JAR_DIR}

javac -d ${CLASS_DIR} `find ${JAVA_DIR} -name '*.java'`
jar cvfm ${JAR_FILE} ${MF_FILE} -C ${CLASS_DIR} .

