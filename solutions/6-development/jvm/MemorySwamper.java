import java.math.BigInteger;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import static java.lang.management.ManagementFactory.getMemoryMXBean;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;

public class MemorySwamper {
    public static void main(String[] args) throws Exception {
        var app  = new MemorySwamper();
        var pool = Executors.newScheduledThreadPool(3);
        var data = Collections.synchronizedSet(new TreeSet<BigInteger>());
        var rand = new Random();

        app.launchReporter(pool, data);
        app.launchAllocator(pool, data, rand);
        app.launchDisposer(pool, data, rand);
    }

    void launchReporter(ScheduledExecutorService pool, Set<BigInteger> data) {
        pool.scheduleAtFixedRate(() -> {
            var MB    = 1.0 / (1024.0 * 1024.0);
            var usage = getMemoryMXBean().getHeapMemoryUsage();
            System.out.printf("[%tT] Memory Used: %.2f MB, Heap: [%.0f / %.0f / %.0f] MB%n",
                    new Date(),
                    usage.getUsed() * MB,
                    usage.getInit() * MB,
                    usage.getCommitted() * MB,
                    usage.getMax() * MB
            );
            System.out.printf("Data: %d elements%n", data.size());
        }, 0, 1, SECONDS);
    }

    void launchAllocator(ScheduledExecutorService pool, Set<BigInteger> data, Random rand) {
        pool.scheduleWithFixedDelay(() -> {
            try {
                var n = rand.nextInt(100_000) + 1;
                while (n-- > 0) {
                    var k = rand.nextInt(1_000) + 1;
                    var x = rand.nextInt(1_000) + 1;
                    var v = BigInteger.ONE;
                    while (x-- > 0) v = v.multiply(BigInteger.valueOf(k));
                    data.add(v);
                }
            } catch (Exception e) {
                System.out.printf("ERROR: %s%n", e);
            }
        }, 0, 97, MILLISECONDS);
    }

    void launchDisposer(ScheduledExecutorService pool, Set<BigInteger> data, Random rand) {
        pool.scheduleWithFixedDelay(() -> {
            var n  = (rand.nextInt(data.size()) + 1);
            var it = data.iterator();
            while (n-- > 0 && it.hasNext()) {
                it.next();
                it.remove();
            }
        }, 19, 97, MILLISECONDS);
    }
}
