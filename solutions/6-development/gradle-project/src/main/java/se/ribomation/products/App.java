package se.ribomation.products;

import se.ribomation.products.domain.Product;
import se.ribomation.products.domain.ProductRepo;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.List;

/**
 * Main entry point of the application.
 */
public class App {
    public static void main(String[] args) throws Exception {
        var app      = new App();
        var csvFile  = app.parseArgs(args);

        var repo = new ProductRepo();
        var products = repo.loadCSV(csvFile);
        app.print(products);

        var file = app.store(products);
        app.size(file);
        app.copy(file);
        app.rename(file);
        app.delete(file);
    }

    /**
     * Gets the CSV file name
     *
     * @param args cli args
     * @return the file name or a default
     */
    public String parseArgs(String[] args) {
        if (args.length == 1) {
            return args[0];
        }
        return "./src/data/products.csv";
    }

    /**
     * Prints the list of products
     *
     * @param products list of products
     */
    public void print(List<Product> products) {
        products.forEach(System.out::println);
    }

    /**
     * Stores the list of products to a file within a TEMP directory.
     *
     * @param products list of products
     * @return path to the file created
     * @throws IOException if it cannot be stored
     */
    public Path store(List<Product> products) throws IOException {
        var dir     = Files.createTempDirectory("products");
        var outFile = Path.of(dir.toString(), "products.txt");
        Files.writeString(outFile, products.toString());
        System.out.printf("written file %s%n", outFile);
        return outFile;
    }

    /**
     * Prints the size of a file
     *
     * @param file file to investigate
     * @throws IOException if it cannot be found
     */
    public void size(Path file) throws IOException {
        var size = Files.size(file);
        System.out.printf("%s: %.3f KBytes%n", file, size / 1024.0);
    }

    /**
     * Makes a copy of the file, in the same directory.
     *
     * @param src file to copy
     * @throws IOException if something goes wrong
     */
    public void copy(Path src) throws IOException {
        var dst = src.getParent().resolve("copy-products.txt");
        Files.copy(src, dst, StandardCopyOption.REPLACE_EXISTING);
        System.out.printf("written file %s%n", dst);
    }

    /**
     * Renames a file, in the same directiry
     *
     * @param src file to rename
     * @throws IOException if something goes wrong
     */
    public void rename(Path src) throws IOException {
        var dst = src.getParent().resolve("move-products.txt");
        Files.move(src, dst, StandardCopyOption.REPLACE_EXISTING);
        System.out.printf("written file %s%n", dst);
    }

    /**
     * Deletes the TEMP directory and all its files
     *
     * @param file file within the TEMP directory
     * @throws IOException if something goes wrong
     */
    public void delete(Path file) throws IOException {
        var dir   = file.getParent();
        var files = dir.toFile().listFiles(File::isFile);
        for (var f : files) {
            Files.deleteIfExists(f.toPath());
            System.out.printf("deleted file %s%n", f);
        }
        Files.delete(dir);
        System.out.printf("deleted dir %s%n", dir);
    }

}
