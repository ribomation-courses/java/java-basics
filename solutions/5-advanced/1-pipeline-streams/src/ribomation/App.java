package ribomation;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class App {
    public static void main(String[] args) throws Exception {
        App app = new App();
        app.run("./data/persons.csv");
    }

    private void run(String filename) throws IOException {
        List<Person> persons = Files
                .lines(Path.of(filename))
                .skip(1)
                .map(line -> line.split(";"))
                .map(arr -> new Person(arr[0], arr[1], arr[2], arr[3]))
                .filter(Person::isFemale)
                .filter(p -> 30 <= p.getAge() && p.getAge() <= 40)
                .filter(p -> p.getPostcode() < 20_000)
                //.peek(System.out::println)
                .collect(Collectors.toList());

        persons.forEach(System.out::println);
    }

}
