package ribomation;

import java.util.StringJoiner;

public class Person {
    private String  name;
    private boolean female;
    private int     age;
    private int     postcode;

    public Person(String name, String female, String age, String postcode) {
        this(name, female.equals("Female"), Integer.parseInt(age), Integer.parseInt(postcode));
    }

    public Person(String name, boolean female, int age, int postcode) {
        this.name = name;
        this.female = female;
        this.age = age;
        this.postcode = postcode;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Person.class.getSimpleName() + "[", "]")
                .add("name='" + name + "'")
                .add("female=" + female)
                .add("age=" + age)
                .add("postcode=" + postcode)
                .toString();
    }

    public String getName() {
        return name;
    }

    public boolean isFemale() {
        return female;
    }

    public int getAge() {
        return age;
    }

    public int getPostcode() {
        return postcode;
    }
}
