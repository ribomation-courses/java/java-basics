package se.ribomation.words;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Random;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;

public class App {
    public static void main(String[] args) throws Exception {
//        var file = "./data/musketeers.txt";
        var file = "./data/shakespeare.txt";
        var app  = new App();
        app.load(file);
    }

    private void load(String file) throws IOException {
        var startTime = System.nanoTime();
        var words = Files
                .lines(Path.of(file))
                .flatMap(line -> Pattern.compile("[^a-zA-Z]+").splitAsStream(line))
                .filter(word -> word.length() > 4)
                .collect(groupingBy(String::toLowerCase, counting()))
                .entrySet().stream()
                .sorted((lhs, rhs) -> (int) (rhs.getValue() - lhs.getValue()))
                .limit(100)
                .collect(toList());

        var maxFreq = words.get(0).getValue();
        var minFreq = words.get(words.size() - 1).getValue();
        var maxFont = 200.0;
        var minFont = 25.0;
        var scale   = (maxFont - minFont) / (maxFreq - minFreq);
        var prefix  = "<html><body>";
        var suffix  = "</body></html>";
        var r       = new Random();
        var html = words.stream()
                .map(pair -> String.format("<span style='font-size:%dpx; color:#%s'>%s</span>",
                        (int) (pair.getValue() * scale + minFont),
                        String.format("%02x%02x%02x",
                                r.nextInt(256), r.nextInt(256), r.nextInt(256)),
                        pair.getKey()))
                .sorted((_a, _b) -> r.nextBoolean() ? -1 : +1)
                .collect(Collectors.joining(System.lineSeparator(), prefix, suffix));

        var outfile = Path.of("./out/words.html");
        Files.writeString(outfile, html);
        System.out.printf("written %s%n", outfile);
        var endTime = System.nanoTime();
        System.out.printf("elapsed %.3f seconds", (endTime - startTime) * 1E-9);
    }

}
