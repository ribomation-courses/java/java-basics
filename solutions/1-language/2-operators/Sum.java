public class Sum {
    public static void main(String[] args) {
        var N = args.length == 0 ? 100L : Long.parseLong(args[0]);
        var S = N * (N + 1) / 2;
        System.out.printf("SUM(1..%d) = %d%n", N, S);
    }
}
