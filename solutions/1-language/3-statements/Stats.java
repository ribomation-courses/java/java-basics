import java.util.Scanner;
public class Stats {
    public static void main(String[] args) {
        final var N = args.length == 0 ? 5 : Integer.parseInt(args[0]);
        var numbers = new int[N];
        var in = new Scanner(System. in);
        for (var k = 0; k < numbers.length; ++ k) {
            System.out.print("Integer? ");
            numbers[k] = in.nextInt();
        }

        // (a) print array
        for (var n: numbers) 
            System.out.println(n);
        

        // (b) sum
        var sum = 0;
        for (var n: numbers) 
            sum += n;
        System.out.printf("SUM = %d%n", sum);

        // (c) product
        var prod = 1;
        for (var n: numbers) 
            prod *= n;
        System.out.printf("PROD = %d%n", prod);

        // (d) min/max val
        var min = Integer.MAX_VALUE;
        var max = Integer.MIN_VALUE;
        for (var n: numbers) {
            min = (n < min) ? n : min;
            max = (n > max) ? n : max;
        }
        System.out.printf("min/max: %d/%d%n", min, max);

        System.out.printf("AVG = %.2f%n", sum / (double)N);
    }
}
