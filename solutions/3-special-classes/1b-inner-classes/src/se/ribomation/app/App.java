package se.ribomation.app;

import se.ribomation.shapes.Shape;

import java.util.Arrays;
import java.util.Comparator;

public class App {
    public static void main(String[] args) {
        var app = new App();
        app.run();
    }

    private void run() {
        var list = Shape.create(10);
        Arrays.sort(list, new BaseComparator() {
            @Override
            public int compare(float leftArea, float rightArea) {
                return Float.compare(rightArea, leftArea);
            }
        });
        print(list);
    }

    private void print(Shape[] shapes) {
        for (Shape s : shapes) {
            System.out.printf("%s: area = %.2f%n",
                    s.getClass().getSimpleName(),
                    s.area());
        }
    }

//    private static class ShapeComparator implements Comparator<Shape> {
//        @Override
//        public int compare(Shape lhs, Shape rhs) {
//            return Double.compare(lhs.area(), rhs.area());
//        }
//    }

    static abstract class BaseComparator implements Comparator<Shape> {
        public abstract int compare(float leftArea, float rightArea);

        @Override
        public final int compare(Shape lhs, Shape rhs) {
            return compare(lhs.area(), rhs.area());
        }
    }
}
