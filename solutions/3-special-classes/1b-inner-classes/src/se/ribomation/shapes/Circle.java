package se.ribomation.shapes;

public class Circle extends Shape {
    private int radius;

    public Circle(int radius) {
        this.radius = radius;
    }

    @Override
    public float area() {
        return (float) (Math.PI * radius * radius);
    }
}
