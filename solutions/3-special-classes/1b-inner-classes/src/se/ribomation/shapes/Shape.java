package se.ribomation.shapes;

import java.util.Random;

public abstract class Shape {
    public abstract float area();

    private static Random r = new Random();

    @Override
    public String toString() {
        return String.format("%s: area=%.1f",
                this.getClass().getSimpleName(),
                this.area());
    }

    public static Shape create() {
        var a = 1 + r.nextInt(10);
        var b = 1 + r.nextInt(10);
        switch (r.nextInt(4)) {
            case 0:
                return new Rectangle(a, b);
            case 1:
                return new Circle(a);
            case 2:
                return new Triangle(a, b);
            case 3:
                return new Square(a);
        }
        return null;
    }

    public static Shape[] create(int n) {
        var result = new Shape[n];
        for (var k = 0; k < result.length; ++k) {
            result[k] = create();
        }
        return result;
    }

}
