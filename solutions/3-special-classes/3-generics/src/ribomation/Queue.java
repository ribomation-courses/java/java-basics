package ribomation;

public class Queue<T> {
    private final T[] storage;
    private int size = 0;
    private int putIndex = 0;
    private int getIndex = 0;

    public Queue(int capacity) {
        storage = (T[]) new Object[capacity];
    }

    public boolean full() {
        return size() == capacity();
    }

    public boolean empty() {
        return size() == 0;
    }

    public int size() {
        return this.size;
    }

    public int capacity() {
        return storage.length;
    }

    public void put(T x) {
        if (full()) {
            throw new RuntimeException("q full");
        }

        storage[putIndex] = x;
        putIndex = (putIndex + 1) % capacity();
        ++size;
    }

    public T get() {
        if (empty()) {
            throw new RuntimeException("q empty");
        }

        var x = storage[getIndex];
        getIndex = (getIndex + 1) % capacity();
        --size;
        return x;
    }
}
