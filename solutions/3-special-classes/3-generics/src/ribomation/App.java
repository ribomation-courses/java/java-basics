package ribomation;

public class App {
    public static void main(String[] args) {
        App app = new App();
        app.qOfInts(10);
        app.qOfStrings(10);
        app.getFromEmptyQ();
        app.putToFullQ();
    }

    void qOfInts(final int N) {
        var q = new Queue<Integer>(N);
        for (var k = 1; !q.full(); ++k) q.put(k);
        while (!q.empty()) System.out.print(q.get() + " ");
        System.out.println();
    }

    void qOfStrings(final int N) {
        var q = new Queue<String>(N);
        for (var k = 1; !q.full(); ++k) q.put("nisse-" + k);
        while (!q.empty()) System.out.print(q.get() + " ");
        System.out.println();
    }

    void getFromEmptyQ() {
        var q = new Queue<String>(1);
        System.out.printf("q.size: %d%n", q.size());
        try {
            q.get();
            throw new RuntimeException("unexpected !!!!");
        } catch (Exception e) {
            System.out.println("expected: " + e);
        }
    }

    void putToFullQ() {
        var q = new Queue<String>(1);
        q.put("one & only");
        System.out.printf("q.size: %d%n", q.size());
        try {
            q.put("ooops");
            throw new RuntimeException("unexpected !!!!");
        } catch (Exception e) {
            System.out.println("expected: " + e);
        }
    }
}
