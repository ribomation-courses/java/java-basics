package ribomation;

public enum Operators {
    plus("+"),
    minus("-"),
    times("*"),
    slash("/"),
    ;

    private final String symbol;

    Operators(String symbol) {
        this.symbol = symbol;
    }

    double eval(double lhs, double rhs) {
        switch (this) {
            case plus: return lhs + rhs;
            case minus: return lhs - rhs;
            case times: return lhs * rhs;
            case slash: return lhs / rhs;
        }
        return 0;
    }

    @Override
    public String toString() { return symbol; }
}
