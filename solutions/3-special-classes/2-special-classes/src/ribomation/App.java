package ribomation;

public class App {
    public static void main(String[] args) {
        App app = new App();
        app.run(40, 2);
    }

    private void run(double lhs, double rhs) {
        for (var op : Operators.values())
            eval(op, lhs, rhs);
    }

    private void eval(Operators op, double lhs, double rhs) {
        System.out.printf("%.1f %s %.1f = %.2f%n",
                lhs, op, rhs, op.eval(lhs, rhs));
    }

}
