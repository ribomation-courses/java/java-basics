package ribomation;

import java.util.Arrays;
import java.util.function.IntUnaryOperator;

public class App {
    public static void main(String[] args) {
        App app = new App();
        app.run();
        System.out.println("---------");
        app.run2();
    }

    void run() {
        Repeat f = (int k) -> System.out.printf("(%d) Java is Cool!%n", k);
        repeat(5, f);
    }

    void run2() {
        var arr = new int[]{1, 2, 3, 4, 5};
        System.out.printf("before: %s%n", Arrays.toString(arr));

        {
            var result = transform(arr, (int k) -> k * k);
            System.out.printf("after: %s%n", Arrays.toString(result));
        }

        {
            var result = transform(arr, (int k) -> k + 42);
            System.out.printf("after: %s%n", Arrays.toString(result));
        }

        {
            var result = transform(arr, new IntUnaryOperator() {
                @Override
                public int applyAsInt(int k) {
                    return k * k - 42;
                }
            });
            System.out.printf("after: %s%n", Arrays.toString(result));
        }

        {
            var result = transform(arr, new Magic());
            System.out.printf("after: %s%n", Arrays.toString(result));
        }
    }

    int[] transform(int[] arr, IntUnaryOperator expr) {
        var result = new int[arr.length];
        for (int k = 0; k < arr.length; k++) {
            result[k] = expr.applyAsInt(arr[k]);
        }
        return result;
    }

    interface Repeat {
        void invoke(int x);
    }

    void repeat(final int n, Repeat expr) {
        for (var i = 1; i <= n; ++i) expr.invoke(i);
    }

    class Magic implements IntUnaryOperator {
        @Override
        public int applyAsInt(int k) {
            return 2 * k;
        }
    }
}
