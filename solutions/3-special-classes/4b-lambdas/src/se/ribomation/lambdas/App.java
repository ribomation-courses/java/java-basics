package se.ribomation.lambdas;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.function.Consumer;
import java.util.function.IntUnaryOperator;

public class App {
    public static void main(String[] args) {
        var app = new App();
        app.run1();
        app.run2();
    }

    void run1() {
        repeat(5, (x) -> System.out.printf("%d) %s%n", x, "Java is cool"));

        final var lst = new ArrayList<Integer>();
        repeat(5, (x) -> lst.add(x * 10));
        System.out.printf("lst: %s%n", lst);
    }

    void run2() {
        int[] lst = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        System.out.printf("before: %s%n", Arrays.toString(lst));

        lst = transform(lst, (x) -> x * x);
        System.out.printf("after: %s%n", Arrays.toString(lst));

        lst = transform(lst, (x) -> 42 + x);
        System.out.printf("after2: %s%n", Arrays.toString(lst));
    }

    void repeat(int n, Consumer<Integer> cons) {
        for (var k = 1; k <= n; ++k) cons.accept(k);
    }

    int[] transform(int[] arr, IntUnaryOperator func) {
        int[] result = new int[arr.length];
        for (var k = 0; k < arr.length; ++k) {
            result[k] = func.applyAsInt(arr[k]);
        }
        return result;
    }
}
