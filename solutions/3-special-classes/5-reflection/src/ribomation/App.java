package ribomation;

import ribomation.domain.Person;

public class App {
    public static void main(String[] args) {
        App app = new App();
        app.run();
    }

    private void run() {
        spy(new Person("Anna", 42, true));
    }

    void spy(Object obj) {
        System.out.printf("obj.fullClass = %s%n", obj.getClass().getName());
        System.out.printf("obj.class = %s%n", obj.getClass().getSimpleName());

        System.out.printf("obj.superClass = %s%n", obj.getClass().getSuperclass().getName());

        for (var m : obj.getClass().getDeclaredMethods()) {
            System.out.printf("obj.method: %s%n", m.getName());
        }

        for (var f : obj.getClass().getDeclaredFields()) {
            System.out.printf("obj.field: %s %s%n", f.getType(), f.getName());
        }
    }
}
