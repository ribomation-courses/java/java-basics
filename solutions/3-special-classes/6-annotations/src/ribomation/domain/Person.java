package ribomation.domain;

import ribomation.nano_spring.Inject;
import ribomation.nano_spring.Bean;

import java.util.Objects;

@Bean
public class Person {
    private String name;
    private Car car;
    @Inject private Cat cat;

    public Person(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format("Person{%s, my car: %s, my cat: %s}",
                name, car, cat);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(name, person.name) &&
                Objects.equals(car, person.car) &&
                Objects.equals(cat, person.cat);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, car, cat);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Car getCar() {
        return car;
    }

    @Inject
    public void setCar(Car mycar) {
        this.car = mycar;
    }

    public Cat getCat() {
        return cat;
    }

    public void setCat(Cat cat) {
        this.cat = cat;
    }
}
