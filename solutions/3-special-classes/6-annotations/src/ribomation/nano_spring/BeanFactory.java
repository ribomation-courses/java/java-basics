package ribomation.nano_spring;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;

public class BeanFactory {
    private final Map<Class, Object> beans = new HashMap<>();

    public BeanFactory(String beanFile) {
        init(beanFile);
    }

    public void init(String beanFile) {
        var in = this.getClass().getResourceAsStream(beanFile);
        if (in == null) {
            throw new RuntimeException("cannot open bean file: " + beanFile);
        }

        try {
            var defs = new Properties();
            defs.load(in);
            defs.forEach((type, value) -> {
                register((String) type, (String) value);
            });
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void register(String className, String initValue) {
        try {
            var cls = Class.forName(className);
            if (!isBean(cls)) {
                throw new RuntimeException("not a bean: " + cls.getName());
            }

            var constr = cls.getConstructor(String.class);
            beans.put(cls, constr.newInstance(initValue));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public Object getBean(Class type) {
        var obj = beans.get(type);
        if (obj == null) {
            throw new RuntimeException("cannot find bean of type: " + type.getName());
        }
        return inject(type, obj);
    }

    private Object inject(Class type, Object obj) {
        injectFields(type, obj);
        injectMethods(type, obj);
        return obj;
    }

    private void injectFields(Class type, Object obj) {
        for (var f : type.getDeclaredFields()) {
            maybeInjected(f)
                    .ifPresent(__ -> injectField(type, f, obj));
        }
    }

    private void injectField(Class type, Field f, Object obj) {
        var beanType = f.getType();
        try {
            f.setAccessible(true);
            f.set(obj, getBean(beanType));
        } catch (IllegalAccessException e) {
            throw new RuntimeException(String.format("failed to wire bean '%s' and field '%s' to bean of type '%s': %s",
                    type.getName(), f, beanType.getName(), e
            ));
        }
    }

    private void injectMethods(Class type, Object obj) {
        for (var m : type.getDeclaredMethods()) {
            maybeInjected(m)
                    .ifPresent(__ -> injectMethod(type, m, obj));
        }
    }

    private void injectMethod(Class type, Method m, Object obj) {
        var beanType = m.getParameterTypes()[0];
        try {
            m.invoke(obj, getBean(beanType));
        } catch (Exception e) {
            throw new RuntimeException(String.format("failed to wire bean '%s' and method '%s' to bean of type '%s': %s",
                    type.getName(), m.getName(), beanType.getName(), e
            ));
        }
    }

    private boolean isBean(Class type) {
        return type.getDeclaredAnnotation(Bean.class) != null;
    }

    private Optional<Inject> maybeInjected(Field f) {
        return Optional.ofNullable(f.getAnnotation(Inject.class));
    }

    private Optional<Inject> maybeInjected(Method m) {
        if (m.getParameterCount() != 1) return Optional.empty();
        return Optional.ofNullable(m.getAnnotation(Inject.class));
    }

}
