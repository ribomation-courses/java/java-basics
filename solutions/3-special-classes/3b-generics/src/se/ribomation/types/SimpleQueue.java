package se.ribomation.types;

public class SimpleQueue<T> {
    private final T[] payload;
    private       int size     = 0;
    private       int putIndex = 0;
    private       int getIndex = 0;

    public SimpleQueue(int capacity) {
        payload = (T[]) new Object[capacity];
    }

    public boolean full() {
        return size == payload.length;
    }

    public boolean empty() {
        return size == 0;
    }

    public void put(T x) {
        payload[putIndex++] = x;
        if (putIndex >= payload.length) putIndex = 0;
        ++size;
    }

    public T get() {
        try {
            return payload[getIndex++];
        } finally {
            if (getIndex >= payload.length) getIndex = 0;
            --size;
        }
    }
}
