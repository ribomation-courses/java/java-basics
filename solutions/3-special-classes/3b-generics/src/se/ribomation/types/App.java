package se.ribomation.types;

import java.util.Calendar;
import java.util.Date;

public class App {
    public static void main(String[] args) {
        var app = new App();
        app.run1();
        app.run2();
        app.run3();
        app.run4();
    }

    void run1() {
        var q = new SimpleQueue<Integer>(5);
        for (var k = 1; !q.full(); ++k)
            q.put(k);
        drain(q);
    }

    void run2() {
        var q = new SimpleQueue<String>(5);
        for (var k = 1; !q.full(); ++k)
            q.put("Nisse-" + k);
        drain(q);
    }

    void run3() {
        var q = new SimpleQueue<Double>(5);
        for (var k = 1; !q.full(); ++k)
            q.put(Math.PI + k);
        drain(q);
    }

    void run4() {
        var q   = new SimpleQueue<Date>(5);
        var cal = Calendar.getInstance();
        for (var k = 1; !q.full(); ++k) {
            cal.add(Calendar.DATE, +1);
            q.put(cal.getTime());
        }
        drain(q);
    }

    <T> void drain(SimpleQueue<T> q) {
        while (!q.empty())
            System.out.printf("q: %s%n", q.get());
    }

}
