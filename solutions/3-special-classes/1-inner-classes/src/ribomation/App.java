package ribomation;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Random;

public class App {
    public static void main(String[] args) {
        App app = new App();
        app.run(args.length == 0 ? 10 : Integer.parseInt(args[0]));
    }

    private void run(final int N) {
        Shape[] shapes = new Shape[N];
        for (var k = 0; k < shapes.length; ++k)
            shapes[k] = mkShape();

        print(shapes);
        System.out.println("---------------");
        Arrays.sort(shapes, new Comparator<Shape>() {
            @Override
            public int compare(Shape lhs, Shape rhs) {
                return Float.compare(lhs.area(), rhs.area());
            }
        }.reversed());
        print(shapes);
    }

    private void print(Shape[] shapes) {
        for (var s : shapes)
            System.out.printf("%s: %.1f%n", s.getClass().getSimpleName(), s.area());
    }

    private Shape mkShape() {
        switch (r.nextInt(3)) {
            case 0:
                return new Rect(r.nextInt(10) + 1, r.nextInt(10) + 1);
            case 1:
                return new Triangle(r.nextInt(10) + 1, r.nextInt(10) + 1);
            case 2:
                return new Circle(r.nextInt(10) + 1);
        }
        return null;
    }

    private final Random r = new Random();

}
