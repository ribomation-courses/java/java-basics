package se.ribomation.products;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

public class App {
    public static void main(String[] args) throws Exception {
        var app      = new App();
        var products = app.load();
        app.print(products);
        var file = app.store(products);
        app.size(file);
        app.copy(file);
        app.rename(file);
        app.delete(file);
    }

    List<Product> load() throws IOException {
        var csvPath  = Path.of("./data/products.csv");
        var products = new ArrayList<Product>();
        Files.readAllLines(csvPath).forEach(csvLine -> {
            if ("name;price;count".equals(csvLine)) return;
            var fields  = csvLine.split(";");
            var product = new Product(fields[0], fields[1], fields[2]);
            products.add(product);
        });
        return products;
    }

    void print(List<Product> products) {
        products.forEach(System.out::println);
    }

    Path store(List<Product> products) throws IOException {
        var dir     = Files.createTempDirectory("products");
        var outFile = Path.of(dir.toString(), "products.txt");
        Files.writeString(outFile, products.toString());
        System.out.printf("written file %s%n", outFile);
        return outFile;
    }

    void size(Path file) throws IOException {
        var size = Files.size(file);
        System.out.printf("%s: %.3f KBytes%n", file, size / 1024.0);
    }

    void copy(Path src) throws IOException {
        var dst = src.getParent().resolve("copy-products.txt");
        Files.copy(src, dst, StandardCopyOption.REPLACE_EXISTING);
        System.out.printf("written file %s%n", dst);
    }

    void rename(Path src) throws IOException {
        var dst = src.getParent().resolve("move-products.txt");
        Files.move(src, dst, StandardCopyOption.REPLACE_EXISTING);
        System.out.printf("written file %s%n", dst);
    }

    void delete(Path file) throws IOException {
        var dir   = file.getParent();
        var files = dir.toFile().listFiles(File::isFile);
        for (var f : files) {
            Files.deleteIfExists(f.toPath());
            System.out.printf("deleted file %s%n", f);
        }
        Files.delete(dir);
        System.out.printf("deleted dir %s%n", dir);
    }

}
