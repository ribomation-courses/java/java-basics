package se.ribomation.products;

import java.util.Comparator;
import java.util.HashSet;
import java.util.TreeMap;
import java.util.TreeSet;

public class App {
    public static void main(String[] args) {
        var app = new App();
        app.run();
    }

    void run() {
        var repo = new ProductRepo();
        var list = repo.create(10);
        list.forEach(System.out::println);

        System.out.println("-".repeat(25));
        Comparator<Product> byPrice = (lhs, rhs) -> Float.compare(rhs.getPrice(), lhs.getPrice());
        list.sort(byPrice);
        list.forEach(System.out::println);

        System.out.println("-".repeat(25));
        var hashSet = new HashSet<>(list);
        hashSet.forEach(System.out::println);

        System.out.println("-".repeat(25));
        var treeSet = new TreeSet<>(list);
        treeSet.forEach(System.out::println);

        System.out.println("-".repeat(25));
        var treeSet2 = new TreeSet<>(byPrice);
        treeSet2.addAll(list);
        treeSet2.forEach(System.out::println);

        System.out.println("-".repeat(25));
        var map = new TreeMap<String, Product>();
        list.forEach(p -> map.put(p.getName(), p));
        map.forEach((name, product) -> System.out.printf("%s: %s%n", name, product));

        System.out.println("-".repeat(25));
        list.removeIf(p -> p.getCount() % 2 == 0);
        list.forEach(System.out::println);
    }
}
