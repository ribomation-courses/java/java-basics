package ribomation.optionals;

import java.util.ArrayList;

public class App {
    public static void main(String[] args) {
        new App().run();
    }

    public void run() {
        OptionalArrayList<String> strings = new OptionalArrayList<>();
        System.out.printf("empty: %s%n", strings.lookup(42).orElse("Ooops"));

        strings.add("one");
        strings.add("two");
        strings.add("three");

        System.out.printf("strings[0]: %s%n", strings.lookup(0).orElse("Ooops"));
        System.out.printf("strings[1]: %s%n", strings.lookup(1).orElse("Ooops"));
        System.out.printf("strings[2]: %s%n", strings.lookup(2).orElse("Ooops"));
        System.out.printf("strings[3]: %s%n", strings.lookup(3).orElse("Ooops"));

        try {
            var str = new ArrayList<String>();
            System.out.println(str.get(42));
        } catch (Exception e) {
            System.out.println("ERR: " + e);
        }
    }

}
