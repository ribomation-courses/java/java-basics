package ribomation.products;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class App {
    public static void main(String[] args) {
        new App().run();
    }

    void run() {
        final int        N        = 1000;
        ProductGenerator gen      = new ProductGenerator();
        List<Product>    products = new ArrayList<>();
        for (int k = 1; k <= N; ++k) {
            products.add(gen.create());
        }

        //products.sort((lhs,rhs) -> Double.compare(rhs.getPrice(), lhs.getPrice()));
        products.sort(Comparator.comparingDouble(Product::getPrice).reversed());

        //all products with a count of 3
        products.removeIf(p -> p.getCount() != 3);

        // and price in [900 .. 1000]
        products.removeIf(p -> !(900 <= p.getPrice() && p.getPrice() <= 1000));

        products.forEach(System.out::println);
    }
}
