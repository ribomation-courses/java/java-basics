package ribomation;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.util.List;

public class App {
    public static void main(String[] args) throws Exception {
        new App().run();
    }

    void run() throws IOException {
        ProductRepo repo = new ProductRepo();

        //step 1: load products
        List<Product> products = repo.load("./src/products.csv");
        //products.forEach(System.out::println);

        //step 2: create tmp dir with tmp file, store all products
        Path tmpDir  = Files.createTempDirectory("products");
        Path dstPath = Path.of(tmpDir.toString(), "products.txt");
        Files.writeString(dstPath, products.toString(), StandardOpenOption.CREATE);
        System.out.printf("written %d bytes to %s%n", Files.size(dstPath), dstPath);

        //step 3: Rename the file and make a copy of it
        Path cpyPath = Path.of(tmpDir.toString(), "products-copy.txt");
        Files.copy(dstPath, cpyPath, StandardCopyOption.REPLACE_EXISTING);
        System.out.printf("written %d bytes to %s%n", Files.size(cpyPath), cpyPath);

        Path movPath = Path.of(tmpDir.toString(), "renamed.txt");
        Files.move(dstPath, movPath, StandardCopyOption.REPLACE_EXISTING);
        System.out.printf("renamed to %s%n", movPath);

        //step 4: Remove the file(s) and the directory
        Files.deleteIfExists(movPath);
        Files.deleteIfExists(cpyPath);
        Files.deleteIfExists(tmpDir);
    }
}
