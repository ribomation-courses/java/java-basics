package ribomation2;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;

public class App {
    public static void main(String[] args) throws IOException {
        var app = new App();
        app.run("./src/products.csv");
    }

    void run(String filename) throws IOException {
        var p = Path.of(filename);
        if (!Files.isReadable(p)) {
            throw new RuntimeException("cannot open file: " + filename);
        }

        var products = new ArrayList<Product>();
        var first    = true;
        for (var csv : Files.readAllLines(p)) {
            if (first) { first = false; continue; }

            var fields = csv.split(";");
            var name   = fields[0];
            var price  = Double.parseDouble(fields[1]);
            var count  = Integer.parseInt(fields[2]);

            products.add(new Product(name, price, count));
        }

        var tmpFile = Files.createTempFile(null, null);
        Files.writeString(tmpFile, products.toString());
        System.out.printf("written to %s%n", tmpFile);
        System.out.printf("file size is %d bytes%n", Files.size(tmpFile));

        var p1 = Files.copy(tmpFile, tmpFile.resolveSibling("whatever"));
        System.out.printf("copied to %s%n", p1);

        var p2 = Files.move(tmpFile, tmpFile.resolveSibling("tjolla-hopp"));
        System.out.printf("moved to %s%n", p2);

        Files.delete(p1);
        Files.delete(p2);
    }
}
