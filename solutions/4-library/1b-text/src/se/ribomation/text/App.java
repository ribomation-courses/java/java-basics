package se.ribomation.text;
import java.util.LinkedHashMap;
import java.util.Map;
import static se.ribomation.text.EmailChecker.isEmail;

public class App {
    public static void main(String[] args) {
        var app = new App();
        app.test();
    }

    void test() {
        for (var data : testData.entrySet()) {
            var input = data.getKey();
            var output = data.getValue();
            if (isEmail(input) == output) {
                System.out.printf("PASSED: '%s'%n", input);
            } else {
                System.out.printf("FAILED: '%s'%n", input);
            }
        }
    }

    final static Map<String, Boolean> testData = new LinkedHashMap<>();
    static {
        testData.put(null, false);
        testData.put("", false);
        testData.put("   ", false);
        testData.put("@foo.bar", false);
        testData.put("   @foo.bar", false);
        testData.put("inge.vidare.foo.bar", false);
        testData.put("inge@vidare@foo.bar", false);
        testData.put("42.vidare@foo.bar", false);
        testData.put("inge.vidare.@fo#o.bar", false);
        testData.put("inge.v!da&e@ribomation.se", false);
        testData.put("inge.vidare@", false);
        testData.put("inge.vidare@42ribomation.se", false);
        testData.put("jens.riboe@ribomation.se", true);
        testData.put("jens.riboe@ribomation.info", false);
    }

}
