package ribomation;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class App {
    public static void main(String[] args) throws Exception {
        var app = new App();
        app.runAll("./data/products.csv");
    }

    void runAll(String filename) throws IOException, ClassNotFoundException {
        var p = Path.of(filename);
        if (!Files.isReadable(p)) {
            throw new RuntimeException("cannot open file: " + filename);
        }

        var products = load(Path.of(filename));
        System.out.printf("loaded %d objects%n", products.size());

        var tmp = Files.createDirectory(Path.of("out/data"));
        storeSerialized(products, tmp);
        storeCompressed(products, tmp);
        Files.deleteIfExists(tmp);
    }

    List<Product> load(Path file) throws IOException {
        return Files.lines(file)
                .skip(1)
                .map(csv -> csv.split(";"))
                .map(fields -> new Product(fields[0], fields[1], fields[2]))
                .collect(Collectors.toList())
                ;
    }

    void storeSerialized(List<Product> products, Path dir) throws IOException, ClassNotFoundException {
        var file = Files.createTempFile(dir, "list", ".ser");
        store(products, file, Files.newOutputStream(file), Files.newInputStream(file));
    }

    void storeCompressed(List<Product> products, Path dir) throws IOException, ClassNotFoundException {
        var file = Files.createTempFile(dir, "list", ".ser.gz");
        store(products, file,
                new GZIPOutputStream(Files.newOutputStream(file)),
                new GZIPInputStream(Files.newInputStream(file)));
    }

    void store(List<Product> payload, Path file, OutputStream os, InputStream is) throws IOException, ClassNotFoundException {
        var oos = new ObjectOutputStream(os);
        try (oos) {
            oos.writeObject(payload);
        }
        System.out.printf("stored file '%s', size: %d bytes%n", file, Files.size(file));

        var ois = new ObjectInputStream(is);
        try (ois) {
            var restored = (List<Product>) ois.readObject();
            if (!payload.equals(restored)) {
                throw new RuntimeException("ooooops, not equals");
            }
        }

        Files.deleteIfExists(file);
    }

}
