package ribomation;

public class Email {
    public static boolean ok(String txt) {
        if (txt == null || txt.trim().length() == 0) return false;

        String[] fields = txt.split("@");
        if (fields.length != 2) return false;

        String name   = fields[0];
        String domain = fields[1];

        if (!Character.isLetter(name.charAt(0))) return false;
        if (name.endsWith(".")) return false;
        if (!name.matches("[a-z0-9.-]+")) return false;

        if (!Character.isLetter(domain.charAt(0))) return false;
        if (domain.endsWith(".")) return false;
        if (!domain.matches("[a-z0-9.]+")) return false;

        String[] fields2 = txt.split("\\.");
        if (fields2.length < 2) return false;

        String tld = fields2[fields2.length - 1];
        if (!(tld.length() == 2 || tld.length() == 3)) return false;

        return true;
    }
}
