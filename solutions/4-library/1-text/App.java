package ribomation;

import java.util.Arrays;
import java.util.List;

public class App {
    public static void main(String[] args) {
        App app = new App();
        app.run();
    }

    private void run() {
        for (var email : testData) {
            System.out.printf("'%s' --> %b%n", email, isEmail(email));
        }
    }

    boolean isEmail(String txt) {
        if (txt == null || txt.trim().length() == 0) {
            return false;
        }

        var parts = txt.split("@");
        if (parts.length != 2) {
            return false;
        }

        String name   = parts[0];
        String domain = parts[1];

        if (name.trim().length() == 0) {
            return false;
        }
        if (!Character.isLetter(name.charAt(0))) {
            return false;
        }
        if (name.charAt(name.length() - 1) == '.') {
            return false;
        }
        if (!name.matches("[a-zA-Z0-9.-]+")) {
            return false;
        }

        if (domain.trim().length() == 0) {
            return false;
        }
        if (!Character.isLetter(domain.charAt(0))) {
            return false;
        }
        if (domain.charAt(domain.length() - 1) == '.') {
            return false;
        }
        if (!domain.matches("[a-zA-Z.]+")) {
            return false;
        }

        var domainParts = domain.split("\\.");
        if (domainParts.length < 2) {
            return false;
        }

        var tld         = domainParts[domainParts.length - 1];
        if (tld.length() != 2 && tld.length() != 3) {
            return false;
        }

        return true;
    }

    String[] testData = {
            null,
            "",
            "   ",
            "@foo.bar",
            "   @foo.bar",
            "inge.vidare.foo.bar",
            "inge@vidare@foo.bar",
            "42.vidare@foo.bar",
            "inge.vidare.@fo#o.bar",
            "inge.v!da&e@ribomation.se",
            "inge.vidare@",
            "inge.vidare@42ribomation.se",
            "inge.vidare@ribomation.se.",
            "inge.vidare@r!bom&tion.se.",
            "jens.riboe@ribomation.comm",
            "jens.riboe@ribomation.c",
            "jens.riboe@ribo.mat.ion.se",
            "jens.riboe@ribomation.se",
    };

}
