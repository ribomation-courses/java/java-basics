package ribomation;
import java.util.*;

public class App {
    public static void main(String[] args) {
        new App().run();
    }

    public void run() {
        System.out.println("--- products ---");
        ProductRepo.instance
                .getProducts()
                .forEach(System.out::println);

        sortInDescPriceOrder(ProductRepo.instance.getProducts());
        copyToHashSet(ProductRepo.instance.getProducts());
        copyToTreeSet(ProductRepo.instance.getProducts());
        copyToTreeSet2(ProductRepo.instance.getProducts());
        copyToMap(ProductRepo.instance.getProducts());
        removeEvenCount(ProductRepo.instance.getProducts());
    }

    void sortInDescPriceOrder(List<Product> products) {
        System.out.println("--- in desc price order ---");
        products.sort((lhs, rhs) -> Double.compare(rhs.getPrice(), lhs.getPrice()));
        products.forEach(System.out::println);
    }

    void copyToHashSet(List<Product> products) {
        System.out.println("--- in hash-set ---");
        HashSet<Product> set = new HashSet<>(products);
        set.forEach(System.out::println);
    }

    void copyToTreeSet(List<Product> products) {
        System.out.println("--- in tree-set ---");
        TreeSet<Product> set = new TreeSet<>(products);
        set.forEach(System.out::println);
    }

    void copyToTreeSet2(List<Product> products) {
        System.out.println("--- in tree-set (desc count order) ---");
        TreeSet<Product> set = new TreeSet<>((lhs, rhs) -> Integer.compare(rhs.getCount(), lhs.getCount()));
        set.addAll(products);
        set.forEach(System.out::println);
    }

    void copyToMap(List<Product> products) {
        System.out.println("--- in tree-map ---");
        Map<String, Product> map = new TreeMap<>();
        for (Product p : products) {
            map.put(p.getName(), p);
        }
        map.forEach((name, product) ->
                System.out.printf("%s: SEK %.2f (%d items)%n",
                        name, product.getPrice(), product.getCount()));
    }

    void removeEvenCount(List<Product> products) {
        System.out.println("--- just odd counts ---");
        products.removeIf(p -> p.getCount() % 2 == 0);
        products.forEach(System.out::println);
    }

}
