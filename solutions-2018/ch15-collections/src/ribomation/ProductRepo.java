package ribomation;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ProductRepo {
    public static final ProductRepo instance = new ProductRepo();

    private List<Product> products = new ArrayList<>();

    public List<Product> getProducts() {
        return products;
    }

    public ProductRepo() {
        try {
            products = Files.lines(Path.of("./src/products.csv"))
                    .skip(1)
                    .map(csv -> csv.split(","))
                    .map(fields -> new Product(
                            fields[0],
                            Double.parseDouble(fields[1]),
                            Integer.parseInt(fields[2])))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
