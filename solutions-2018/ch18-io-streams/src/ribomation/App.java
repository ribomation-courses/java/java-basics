package ribomation;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class App {
    public static void main(String[] args) throws Exception {
        App           app      = new App();
        List<Product> products = new ProductRepo().load("./src/products.csv");
        Path          tmpDir   = Files.createTempDirectory("products");

        app.saveAsSerializedAndRestore(products, tmpDir);
        app.saveAsCompressedAndRestore(products, tmpDir);
    }


    void saveAsSerializedAndRestore(List<Product> products, Path dir) throws IOException, ClassNotFoundException {
        Path dst = Path.of(dir.toString(), "products.ser");

        //phase 1: store the serialized data
        ObjectOutputStream out =
                new ObjectOutputStream(
                        Files.newOutputStream(dst, StandardOpenOption.CREATE));
        try (out) {
            out.writeObject(products);
        }
        System.out.printf("written %d bytes to %s%n", Files.size(dst), dst);

        //phase 2: re-store the serialized data
        ObjectInputStream in =
                new ObjectInputStream(
                        Files.newInputStream(dst));
        List<Product> restored;
        try (in) {
            restored = (List<Product>) in.readObject();
        }

        verify(products, restored);
    }

    void saveAsCompressedAndRestore(List<Product> products, Path dir) throws IOException, ClassNotFoundException {
        Path dst = Path.of(dir.toString(), "products.gz");

        //phase 1: store the serialized data
        ObjectOutputStream out =
                new ObjectOutputStream(
                        new GZIPOutputStream(
                                Files.newOutputStream(dst, StandardOpenOption.CREATE)));
        try (out) {
            out.writeObject(products);
        }
        System.out.printf("written %d bytes to %s%n", Files.size(dst), dst);

        //phase 2: re-store the serialized data
        ObjectInputStream in =
                new ObjectInputStream(
                        new GZIPInputStream(
                                Files.newInputStream(dst)));
        List<Product> restored;
        try (in) {
            restored = (List<Product>) in.readObject();
        }

        verify(products, restored);
    }

    void verify(List<Product> products, List<Product> restored) {
        if (products.equals(restored)) {
            System.out.println("successfully restored the list of products");
        } else {
            System.out.printf("failed restore: %d items vs %d items%n",
                    products.size(), restored.size());
        }
    }

}
