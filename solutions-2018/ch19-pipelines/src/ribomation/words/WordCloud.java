package ribomation.words;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class WordCloud {

    public static final String PREFIX = "<html><body>";
    public static final String SUFFIX = "</body></html>";

    public static void main(String[] args) throws Exception {
        WordCloud app = new WordCloud();
        //app.run("./src/shakespeare.txt");
        app.run("./src/musketeers.txt");
    }

    int maxFont = 150;
    int minFont = 10;

    public void run(String filename) throws IOException {
        long startTime = System.nanoTime();

        Map<String, Long> freqs = Files
                .lines(Path.of(filename))
                .flatMap(line -> Pattern.compile("[^a-zA-Z]+").splitAsStream(line))
                .filter(word -> !word.isBlank())
                .filter(word -> word.length() > 3)
                .collect(Collectors.groupingBy(
                        String::toLowerCase,
                        Collectors.counting()));

        List<Map.Entry<String, Long>> tags = freqs.entrySet().stream()
                .sorted((lhs, rhs) -> Long.compare(rhs.getValue(), lhs.getValue()))
                .limit(100)
                .collect(Collectors.toList());

        long   maxFreq = tags.get(0).getValue();
        long   minFreq = tags.get(tags.size() - 1).getValue();
        double scale   = (double) (maxFont - minFont) / (maxFreq - minFreq);

        Random r = new Random();
        String tagsHtml = tags.stream()
                .map(pair -> String.format("<span style='font-size: %dpt; padding-right: 1rem; color: #%2x%2x%2x'>%s</span>",
                        (int) (scale * pair.getValue()) + minFont,
                        r.nextInt(256), r.nextInt(256), r.nextInt(256),
                        pair.getKey()))
                .sorted((_lhs, _rhs) -> r.nextBoolean() ? -1 : +1)
                .collect(Collectors.joining(System.lineSeparator(), PREFIX, SUFFIX));

        Files.writeString(Path.of("./word-cloud.html"), tagsHtml, StandardOpenOption.CREATE);
        long   endTime = System.nanoTime();
        double elapsed = (endTime - startTime) * 1E-9;
        System.out.printf("Elapsed %.3f seconds%n", elapsed);
    }

}
