package ribomation.persons;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class App {
    public static void main(String[] args) throws Exception {
        App app = new App();
        app.run();
    }

    public void run() throws IOException {

        List<Person> people = Files
                .lines(Path.of("./src/persons.csv"))
                .skip(1)
                //.limit(25)
                //.peek(System.out::println)
                .map(csv -> csv.split(";"))
                //.peek(fields -> System.out.println(Arrays.toString(fields)))
                .map(fields -> new Person(
                        fields[0],
                        fields[1].equals("Female"),
                        Integer.parseInt(fields[2]),
                        Integer.parseInt(fields[3])
                ))
                //.peek(System.out::println)
                .filter(Person::isFemale)
                //.peek(System.out::println)
                .filter(p -> 30 <= p.getAge() && p.getAge() <= 40)
                //.peek(System.out::println)
                .filter(p -> p.getPostCode() < 20000)
                //.peek(System.out::println)
                .collect(Collectors.toList());

        people.forEach(System.out::println);
    }

}
