package ribomation.persons;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class App2 {
    public static void main(String[] args) throws IOException {
        App2 app = new App2();
        app.run();
    }

    void run() throws IOException {
        List<Person> list = extractPersons("./src/persons.csv", ";");
        list.forEach(System.out::println);
    }

    List<Person> extractPersons(String filename, String delim) throws IOException {
        return Files.lines(Path.of(filename))
                .skip(1)
                //.limit(20)
                .map(csv -> Arrays.asList(csv.split(delim)))
                .map(f -> new Person(
                        f.get(0),
                        f.get(1).equals("Female"),
                        Integer.parseInt(f.get(2)),
                        Integer.parseInt(f.get(3))
                ))
                .filter(Person::isFemale)
                .filter(p -> 30 <= p.getAge() && p.getAge() <= 40)
                .filter(p -> p.getPostCode() < 20_000)
                //.peek(System.out::println)
                .collect(Collectors.toList());
    }
}
