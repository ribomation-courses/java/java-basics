package ribomation;

import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.Date;

public class App {
    public static void main(String[] args) {
        App app = new App();
        app.testIntQueue();
        app.testStringQueue();
        app.testDateQueue();
    }

    private void testIntQueue() {
        SimpleQueue<Integer> queue = new SimpleQueue<>(5);
        for (int k = 1; !queue.isFull(); ++k) {
            queue.put(k * 10);
        }
        while (!queue.isEmpty()) {
            System.out.printf("%d ", queue.get());
        }
        System.out.println();
    }

    private void testStringQueue() {
        SimpleQueue<String> queue = new SimpleQueue<>(5);
        for (int k = 1; !queue.isFull(); ++k) {
            queue.put("txt-" + k);
        }
        while (!queue.isEmpty()) {
            System.out.printf("%s ", queue.get());
        }
        System.out.println();
    }

    private void testDateQueue() {
        SimpleQueue<Date> queue = new SimpleQueue<>(5);
        LocalDate now = LocalDate.now();
        for (int k = 1; !queue.isFull(); ++k) {
            Date date = Date.from(now.plusDays(k-1).atStartOfDay().toInstant(ZoneOffset.UTC));
            queue.put(date);
        }
        while (!queue.isEmpty()) {
            System.out.printf("%s%n", queue.get());
        }
    }
}
