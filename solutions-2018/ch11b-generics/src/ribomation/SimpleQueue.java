package ribomation;

public class SimpleQueue<ElemType> {
    private ElemType[] arr;
    private int        putIdx = 0;
    private int        getIdx = 0;
    private int        size   = 0;

    public SimpleQueue(int capacity) {
        arr = (ElemType[]) new Object[capacity];
    }

    void put(ElemType x) {
        arr[putIdx] = x;
        putIdx = (putIdx + 1) % capacity();
        ++size;
    }

    ElemType get() {
        ElemType x = arr[getIdx];
        getIdx = (getIdx + 1) % capacity();
        --size;
        return x;
    }

    int size() {
        return this.size;
    }

    int capacity() {
        return arr.length;
    }

    boolean isEmpty() {
        return size() == 0;
    }

    boolean isFull() {
        return size() == capacity();
    }

}
