package ribomation;

public class ExceptionsDemo {
    public static void main(String[] args) {
        System.out.println("ExceptionsDemo.main");
        ExceptionsDemo app = new ExceptionsDemo();
        app.run();
    }

    public void run() {
        System.out.println("ExceptionsDemo.run");
        try {
            func1();
        } catch (Exception e) {
            System.out.printf("ooops we got an error: %s%n", e);
        }
    }

    void func1() throws Exception {
        System.out.println("ExceptionsDemo.func1");
        func2();
        System.out.println("AFTER ExceptionsDemo.func1");
    }

    void func2() throws Exception {
        System.out.println("ExceptionsDemo.func2");
        func3();
        System.out.println("AFTER ExceptionsDemo.func2");
    }

    void func3() throws Exception {
        System.out.println("ExceptionsDemo.func3");
        Object obj = null;
        //System.out.println(obj.toString());
//        throw new NullPointerException();
        throw new Exception("this is an ERROR");
        //System.out.println("AFTER ExceptionsDemo.func3");
    }
}
