package ribomation;
import java.time.LocalDate;

public class App {
    public static void main(String[] args) {
        App app = new App();
        app.runInts();
        app.runStrings();
        app.runDate();
    }

    void runInts() {
        SimpleQueue<Integer> queue = new SimpleQueue<>(5);
        for (int k = 1; !queue.isFull(); ++k) queue.put(k);
        drain(queue);
    }

    void runStrings() {
        SimpleQueue<String> queue = new SimpleQueue<>(5);
        for (int k = 1; !queue.isFull(); ++k) queue.put("str-" + k);
        drain(queue);
    }

    void runDate() {
        SimpleQueue<LocalDate> queue = new SimpleQueue<>(5);
        for (int k = 1; !queue.isFull(); ++k) queue.put(LocalDate.now().minusDays(k));
        drain(queue);
    }

    <T> void drain(SimpleQueue<T> queue) {
        while (!queue.isEmpty()) System.out.print(queue.get() + " ");
        System.out.println();
    }

}
