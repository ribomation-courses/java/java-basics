package ribomation;

public class SimpleQueue<ElemType> {
    private ElemType[] arr;
    private int size = 0;
    private int putIdx = 0;
    private int getIdx = 0;

    public SimpleQueue(int capacity) {
        arr = (ElemType[]) new Object[capacity];
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public boolean isFull() {
        return size == arr.length;
    }

    public void put(ElemType x) {
        arr[putIdx] = x;
        putIdx = (putIdx + 1) % arr.length;
        ++size;
    }

    public ElemType get() {
        try {
            return arr[getIdx];
        } finally {
            getIdx = (getIdx + 1) % arr.length;
            --size;
        }
    }
}


