package ribomation;

public class Wrappers {
    public static void main(String[] args) {
        Wrappers app = new Wrappers();
        app.run();
    }

    private void run() {
        String txt     = "42";
        int    anInt = Integer.parseInt(txt);
        System.out.println("anInt = " + anInt);

        Integer integer = Integer.valueOf(anInt);
        System.out.println("integer = " + integer);

        double doubleValue = integer.doubleValue();
        System.out.println("doubleValue = " + doubleValue);

        System.out.printf("Int: [%d / %d]%n",
                Integer.MIN_VALUE, Integer.MAX_VALUE);
        System.out.printf("Byte: [%d / %d]%n",
                Byte.MIN_VALUE, Byte.MAX_VALUE);
        System.out.printf("Short: [%d / %d]%n",
                Short.MIN_VALUE, Short.MAX_VALUE);
    }
}
