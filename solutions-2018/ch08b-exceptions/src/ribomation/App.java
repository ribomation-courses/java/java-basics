package ribomation;

public class App {
    public static void main(String[] args) {
        App app = new App();
        app.run();
    }

    private void run() {
        System.out.println("App.run");
        try {
            func1();
        } catch (Exception e) {
            System.out.printf("ERROR: %s%n", e);
        }
        otherFunc();
        System.out.println("App.run exit");
    }

    void func1() {
        System.out.println("App.func1");
        func2();
        System.out.println("App.func1 exit");
    }

    void func2() {
        System.out.println("App.func2");
        func3();
        System.out.println("App.func2 exit");
    }

    void func3() {
        System.out.println("App.func3");
        func4();
        System.out.println("App.func3 exit");
    }

    void func4() {
        System.out.println("App.func4");
        Object ptr = null;
        System.out.println(ptr.toString());
        System.out.println("App.func4 exit");
    }

    void otherFunc() {
        System.out.println("App.otherFunc");
    }

}
