package ribomation;

public class Rect extends Shape {
    protected int width, height;

    public Rect(int width, int height) {
        this.width = width;
        this.height = height;
    }

    @Override
    public double area() {
        return width * height;
    }

    @Override
    public String toString() {
        return String.format("Rect{w=%d, h=%d, area=%.2f}",
                width, height, area());
    }
}
