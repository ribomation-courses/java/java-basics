package ribomation;

public class Square extends Rect {
    public Square(int side) {
        super(side, side);
    }

    @Override
    public String toString() {
        return String.format("Square{s=%d, area=%.2f}", height, area());
    }
}
