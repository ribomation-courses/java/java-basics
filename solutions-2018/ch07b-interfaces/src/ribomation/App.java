package ribomation;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Random;

public class App {
    public static void main(String[] args) {
        App app = new App();
        app.run();
    }

    private void run() {
        Shape[] shapes = create(1000);

        Arrays.sort(shapes, new Comparator<Shape>() {
            @Override
            public int compare(Shape lhs, Shape rhs) {
                return Double.compare(rhs.area(), lhs.area());
            }
        });

        for (Shape s : shapes) System.out.println(s);
    }

    private Random r = new Random();

    Shape[] create(int n) {
        Shape[] shapes = new Shape[n];
        for (int k = 0; k < shapes.length; k++) shapes[k] = create();
        return shapes;
    }

    Shape create() {
        switch (r.nextInt(3)) {
            case 0:
               return new Rect(r.nextInt(35) + 1, r.nextInt(10) + 1);
            case 1:
               return new Circle(r.nextInt(10) + 1);
            case 2:
                return new Square(r.nextInt(10) + 1);
        }
        return null; //cannot happen
    }

}
