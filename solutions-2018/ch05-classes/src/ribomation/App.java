package ribomation;

public class App {
    public static void main(String[] args) {
        App app = new App();
        app.run();
    }

    private void run() {
        Car volvo = new Car("ABC123", "XC90");

        System.out.println("volvo = " + volvo);
        System.out.printf("volvo.license = %s%n", volvo.getLicense());
        System.out.printf("volvo.model   = %s%n", volvo.getModel());

        volvo.setModel("Amazon");
        System.out.println("volvo = " + volvo);
        System.out.printf("volvo.model   = %s%n", volvo.getModel());

        Person nisse = new Person("Nisse");
        System.out.printf("nisse: %s%n", nisse);

        nisse.connect(volvo);
        System.out.printf("nisse: %s%n", nisse);

        Person anna = new Person("Anna");
        System.out.printf("anna: %s%n", anna);
        anna.connect(new Car("AAA222", "S90"));
        System.out.printf("anna: %s%n", anna);
        System.out.printf("model: %s%n", anna.getMyCar().getModel());
    }
}
