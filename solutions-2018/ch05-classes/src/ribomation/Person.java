package ribomation;

public class Person {
    private String name;
    private Car    myCar;

    public Person(String name) {
        this.name = name;
    }

    public void connect(Car car) {
        this.myCar = car;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", myCar=" + myCar +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Car getMyCar() {
        return myCar;
    }

}
