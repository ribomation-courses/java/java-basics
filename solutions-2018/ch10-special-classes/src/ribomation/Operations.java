package ribomation;

public enum Operations {
    ADD("+"), SUB("-"), MUL("*"), DIV("/");

    final String symbol;

    Operations(String symbol) {
        this.symbol = symbol;
    }

    int eval(int lhs, int rhs) {
        switch (this) {
            case ADD:
                return lhs + rhs;
            case SUB:
                return lhs - rhs;
            case MUL:
                return lhs * rhs;
            case DIV:
                return lhs / rhs;
        }
        return 0;
    }
}
