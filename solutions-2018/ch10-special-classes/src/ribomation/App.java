package ribomation;
import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        new App().run();
    }

    public void run() {
        System.out.print("Give 2 ints: ");
        Scanner in  = new Scanner(System.in);
        int     lhs = in.nextInt();
        int     rhs = in.nextInt();

        for (Operations OP : Operations.values()) {
            System.out.printf("%d %s %d = %d%n", lhs, OP.symbol, rhs, OP.eval(lhs, rhs));
        }
    }
}
