package ribomation;

import java.util.Locale;

public class Circle extends Shape {
    private int radius;

    public Circle(int radius) {
        this.radius = radius;
    }

    @Override
    public double area() {
        return Math.PI * radius * radius;
    }

    @Override
    public String toString() {
/*
        return "Circle{" +
                "radius=" + radius +
                ", area=" + area() +
                '}';
*/
        return String.format("Circle{radius=%d, , area=%.2f}",
                radius, area());
    }
}
