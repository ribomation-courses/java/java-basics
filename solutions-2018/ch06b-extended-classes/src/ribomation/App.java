package ribomation;

import java.util.Arrays;
import java.util.Locale;

public class App {
    public static void main(String[] args) {
        //Locale.setDefault(Locale.ENGLISH);
        App app = new App();
        app.run();
    }

    private void run() {
        Shape[] shapes = new Shape[]{
                new Rect(3, 4), new Rect(2, 5),
                new Triangle(4, 7), new Triangle(1, 7),
                new Circle(5), new Circle(2),
        };
        //System.out.println(Arrays.toString(shapes));

        for (Shape s : shapes) {
            System.out.println(s);
        }
    }
}
