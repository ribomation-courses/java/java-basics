import java.util.Scanner;

public class Numbers {
	public static void main(String[] args) {
		Scanner in      = new Scanner(System.in);
		int[]   numbers = new int[5];
		
		System.out.printf("Type %d integers: ", numbers.length);
		for (int k=0; k<numbers.length; ++k)
			numbers[k] = in.nextInt();
		
		for (int n : numbers) System.out.print(n + " ");
		System.out.println();
		
		int sum = 0;
		for (int n : numbers) sum += n;  // sum = sum + n
		System.out.printf("SUM(numbers) = %d%n", sum);

		int prod = 1;
		for (int n : numbers) prod *= n;  //prod = prod * n
		System.out.printf("PROD(numbers) = %d%n", prod);

		int min = Integer.MAX_VALUE;
		for (int n : numbers) min = n < min ? n : min;

		int max = Integer.MIN_VALUE;
		for (int n : numbers) max = n > max ? n : max;

		System.out.printf("MIN/MAX = %d/%d%n", min, max);
						
		System.out.printf("AVG(numbers) = %.3f%n", 
			(float)sum/numbers.length);

	}
}
