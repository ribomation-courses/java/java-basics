package ribomation;

import org.junit.Test;

import static org.junit.Assert.*;

public class StringUtilsTest {

    @Test
    public void checks_correct_email_address() {
        assertTrue(StringUtils.isEmail("jens@ribomation.se"));
    }

    @Test
    public void checks_invalid_email_address() {
        assertFalse(StringUtils.isEmail("@ribomation.se"));
        assertFalse(StringUtils.isEmail("jens@riboe@ribomation.se"));
        assertFalse(StringUtils.isEmail("jens.@ribomation.se"));
        assertFalse(StringUtils.isEmail("jens.rib@ribomation."));
        assertFalse(StringUtils.isEmail("42.rib@ribomation.se"));
    }

}