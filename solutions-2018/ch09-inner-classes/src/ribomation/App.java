package ribomation;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Random;

public class App {
    public static void main(String[] args) {
        new App().run();
    }

    void run() {
        Shape[] shapes = mkShapes(20);

        Arrays.sort(shapes, new Comparator<Shape>() {
            @Override
            public int compare(Shape lhs, Shape rhs) {
                return Double.compare(rhs.area(), lhs.area());
            }
        });

        for (Shape s : shapes) System.out.printf("%s%n", s);
    }

    Shape[] mkShapes(int n) {
        Shape[] arr = new Shape[n];
        for (int k = 0; k < arr.length; ++k) arr[k] = mkShape();
        return arr;
    }

    Shape mkShape() {
        int param1 = 1 + r.nextInt(10);
        int param2 = 3 + r.nextInt(10);
        switch (r.nextInt(3)) {
            case 0: return new Rect(param1, param2);
            case 1: return new Circle(param1);
            case 2: return new Triangle(param1,param2);
        }
        return null;
    }

    private Random r = new Random();
}
