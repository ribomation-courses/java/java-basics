package ribomation;

public class Rect extends Shape {
    private int width;
    private int height;

    public Rect(int width, int height) {
        this.width = width;
        this.height = height;
    }

    @Override
    public double area() {
        return width * height;
    }

    @Override
    protected String type() {
        return "Rectangle";
    }
}
