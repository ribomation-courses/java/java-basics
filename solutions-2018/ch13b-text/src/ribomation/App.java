package ribomation;

import java.util.Arrays;
import java.util.List;

public class App {
    public static void main(String[] args) {
        App app = new App();
        app.run();
    }

    private void run() {
        testData.forEach(txt -> System.out.printf("%s --> %b%n", txt, Email.ok(txt)));
    }

    List<String> testData = Arrays.asList(
            "jens.riboe@ribomation.se",
            "jens.riboe@ribomation.comm",
            "jens.riboe@ribomation.c",
            "inge.vidare@ribomation.se.",
            "inge.vid!are@ribomation.se",
            "inge.vidare@ribomation",
            "inge.vidare@42ribomation.se",
            "inge.vidare@",
            "inge.vidare.@fo#o.bar",
            "42.vidare@foo.bar",
            "inge@vidare@foo.bar",
            "",
            null
    );

}
