package ribomation;

public class App {
    public static void main(String[] args) {
        App app = new App();
        app.run();
    }

    private void run() {
        Person person = new Person("Ana Gram", 37);
        print(person);

        Car amazon = new Car("Volvo Amazon").setOwner(person);
        System.out.printf("car: %s%n", amazon);
    }

    private void print(Person p) {
        System.out.printf("person: %s%n", p);
    }

}
