package ribomation;

public class Car {
    private String model;
    private Person owner;

    public Car(String model) {
        this.model = model;
    }

    @Override
    public String toString() {
        return "Car{" +
                "model:'" + model + '\'' +
                ", owner:" + (owner != null ? owner : "-") +
                '}';
    }

    public Car setOwner(Person owner) {
        this.owner = owner;
        return this;
    }

    public Person getOwner() {
        return owner;
    }
}
