package ribomation;

import java.util.Arrays;

public class App {
    public static void main(String[] args) {
        App app = new App();
        app.testRepeat();
        app.testTransform();
        app.testTransform2();
    }

    private void testTransform2() {
        String[] txt = {"hej", "hopp", "tjabba", "tjolla hopp"};
        String[] result = Lambdas.transform(txt, String::toUpperCase);
        System.out.println(Arrays.toString(result));
    }

    private void testTransform() {
        Integer[] numbers = {1, 2, 3, 4, 5, 6, 7, 8};
        Integer[] result  = Lambdas.transform(numbers, n -> n * n);
        System.out.println(Arrays.toString(result));
    }

    private void testRepeat() {
        Lambdas.repeat(5, (k) -> System.out.printf("(%d) Java is Cool%n", k));
    }

}
