package ribomation;


import java.util.function.Consumer;
import java.util.function.Function;

public class Lambdas {

    public static void repeat(int count, Consumer<Integer> func) {
        for (int k = 1; k <= count; ++k) {
            func.accept(k);
        }
    }

    public  static <T> T[] transform(T[] arr, Function<T, T> func) {
        for (int k = 0; k < arr.length; ++k) {
            arr[k] = func.apply(arr[k]);
        }
        return arr;
    }
}
