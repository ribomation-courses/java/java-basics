package ribomation;

public abstract class Shape {
    public abstract double area();
    protected abstract String type() ;

    @Override
    public String toString() {
        return String.format("%s{area=%.2f}", type(), area());
    }
}
