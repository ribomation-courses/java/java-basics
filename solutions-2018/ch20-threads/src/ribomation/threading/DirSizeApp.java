package ribomation.threading;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Locale;
import java.util.concurrent.ForkJoinPool;

public class DirSizeApp {
    public static void main(String[] args) throws IOException {
        String myDir = "../../../../../../";
//        String myDir = "C:\\Users\\jensr\\Dropbox\\Ribomation\\Ribomation-Training-2017-Autumn\\java";
        new DirSizeApp().run(new File(myDir));
    }

    void run(File dir) throws IOException {
        System.out.printf("Scanning dir %s...%n",
                dir.getCanonicalFile());

        long   start  = System.nanoTime();
        Result result = new ForkJoinPool(10).invoke(new ComputeTask(Paths.get(dir.getAbsolutePath())));
        long   end    = System.nanoTime();

        System.out.printf(Locale.ENGLISH,
                "DIR: %s%n%s%nElapsed Time = %.3f secs%n",
                dir.getCanonicalPath(), result, (end - start) * 1E-9);
    }
}
