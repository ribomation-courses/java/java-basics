package ribomation.threading;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;

public class ComputeTask extends RecursiveTask<Result> {
    private Path dir;

    public ComputeTask(Path dirPath) {
        if (!Files.isDirectory(dirPath)) {
            throw new IllegalArgumentException("not a directory: "
                    + dir.toAbsolutePath().toString());
        }
        this.dir = dirPath;
    }

    @Override
    protected Result compute() {
        Result            result = new Result();
        List<ComputeTask> tasks  = new ArrayList<>();

        try {
            Files.list(dir).forEach(p -> {
                if (Files.isRegularFile(p)) {
                    try {
                        result.numFiles++;
                        result.numBytes += Files.size(p);
                    } catch (IOException e) { }
                } else if (Files.isDirectory(p)) {
                    result.numDirs++;
                    ComputeTask task = (ComputeTask) new ComputeTask(p).fork();
                    tasks.add(task);
                }
            });
        } catch (IOException e) {}

        return tasks.stream()
                .map(ForkJoinTask::join)
                .collect(() -> result, Result::add, Result::add);
    }
}
