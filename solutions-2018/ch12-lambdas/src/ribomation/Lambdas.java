package ribomation;

import java.util.Arrays;
import java.util.function.Consumer;
import java.util.function.IntUnaryOperator;

public class Lambdas {
    public static void main(String[] args) {
        Lambdas app = new Lambdas();
        app.run();
    }

    void run() {
        repeat(5, k -> System.out.printf("(%d) Java is cool%n", k));
        repeat(10, n -> System.out.printf("%d * %d = %d%n", n, n, n * n));

        int[] result = transform(new int[]{1, 2, 3, 4, 5}, k -> k * k);
        System.out.printf("result: %s%n", Arrays.toString(result));
    }

    void repeat(int count, Consumer<Integer> stmts) {
        for (int k = 1; k <= count; ++k) {
            stmts.accept(k);
        }
    }

    int[] transform(int[] arr, IntUnaryOperator f) {
        int[] result = new int[arr.length];
        for (int k = 0; k < arr.length; k++) {
            result[k] = f.applyAsInt(arr[k]);
        }
        return result;
    }

}
