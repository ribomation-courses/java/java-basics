package ribomation;

public class TestApp {
    public static void main(String[] args) {
        new TestApp().run();
    }

    void run() {
        for (String email : emails) {
            System.out.printf("\"%s\" --> %b%n",
                    email, StringUtils.isEmail(email));
        }
    }

    final String[] emails = {
            "jens.riboe@ribomation.se",
            "jens@ribomation.se",
            "jens#riboe@ribomation.se",
            "jens.riboe@ribomation.info",
            "jens.riboe@ribomation.",
            "jens.riboe@",
            "jens",
            "jens.@ribomation.se",
            "42@ribomation.se",
            "jens@ribomation@se",
            "",
            null
    };
}
