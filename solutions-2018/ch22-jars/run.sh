#!/usr/bin/env bash
set -ex

rm -rf bld
mkdir -p bld/classes
javac -d bld/classes src/StringUtils.java
javac -d bld/classes -cp bld/classes src/TestApp.java

jar cvfm bld/app.jar src/MANIFEST.MF -C bld/classes .
jar tvf bld/app.jar
echo '----------'
java -cp bld/app.jar ribomation.TestApp
echo '----------'
java -jar bld/app.jar
