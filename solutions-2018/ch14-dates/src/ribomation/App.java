package ribomation;

import java.time.LocalDate;
import java.time.Month;
import java.time.temporal.TemporalAdjuster;
import java.time.temporal.TemporalAdjusters;

public class App {
    public static void main(String[] args) {
        App app = new App();
        app.run();
    }

    private void run() {
        lastDayOfMonth(LocalDate.now());
        lastDayOfMonth(LocalDate.of(2016, Month.FEBRUARY, 15));
        lastDayOfMonth(LocalDate.of(2019, Month.FEBRUARY, 15));
    }

    void lastDayOfMonth(LocalDate date) {
        System.out.println("lastDay = " + date.with(TemporalAdjusters.lastDayOfMonth()));
    }

}
