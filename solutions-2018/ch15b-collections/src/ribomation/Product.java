package ribomation;

public class Product implements Comparable<Product> {
    private String name;
    private float  price;
    private int    count;

    public Product(String name, float price, int count) {
        this.name = name;
        this.price = price;
        this.count = count;
    }

    @Override
    public int compareTo(Product rhs) {
        return this.name.compareTo(rhs.name);
    }

    @Override
    public String toString() {
        return String.format("Product{%s, %.2f kr, (%d)}",
                name, price, count);
    }

    public String getName() {
        return name;
    }

    public float getPrice() {
        return price;
    }

    public int getCount() {
        return count;
    }
}
