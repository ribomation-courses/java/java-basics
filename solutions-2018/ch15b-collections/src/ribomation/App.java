package ribomation;

import java.util.*;
import java.util.function.BiFunction;

public class App {
    public static void main(String[] args) {
        App app = new App();
        app.run();
    }

    void run() {
        List<Product> products = create(10);
        products.forEach(prod -> System.out.println(prod));

        //(1) Sort them in descending price order
        System.out.println("-".repeat(30));
        products.sort((lhs, rhs) ->
                Float.compare(rhs.getPrice(), lhs.getPrice()));
        products.forEach(System.out::println);

        //(2) Copy them into a hash set
        System.out.println("-".repeat(30));
        Set<Product> hashSet = new HashSet<>(products);
        hashSet.forEach(System.out::println);

        // (3) Copy them into a tree set, stored in name order
        System.out.println("-".repeat(30));
        Set<Product> treeSet = new TreeSet<>(products);
        treeSet.forEach(System.out::println);

        // (4) Copy them into a tree set, stored in descending count order
        System.out.println("-".repeat(30));
        Set<Product> treeSet2 = new TreeSet<>(
                Comparator.comparingInt(Product::getCount).reversed());
        treeSet2.addAll(products);
        treeSet2.forEach(System.out::println);

        // (5) Copy them into a map, where its name is the key
        System.out.println("-".repeat(30));
        Map<String, Product> map = new TreeMap<>();
        products.forEach(prod -> map.put(prod.getName(), prod));
        map.forEach((name, prod) -> System.out.printf("%s: %s%n", name, prod));

        // (6) Remove all products with an even count
        System.out.println("-".repeat(30));
        products.removeIf(prod -> prod.getCount() % 2 == 0);
        products.forEach(System.out::println);
    }

    List<Product> create(int n) {
        List<Product> lst = new ArrayList<>(n);
        for (int k = 0; k < n; ++k) {
            lst.add(create());
        }
        return lst;
    }

    Product create() {
        String name  = "prod-" + (10 + r.nextInt(90));
        float  price = r.nextFloat() * 100 + 1;
        int    count = r.nextInt(20);
        return new Product(name, price, count);
    }

    private Random r = new Random();
}
