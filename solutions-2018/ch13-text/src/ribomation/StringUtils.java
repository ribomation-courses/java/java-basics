package ribomation;

public class StringUtils {
    public static boolean isEmail(String s) {
        if (s == null) return false;
        if (s.trim().length() == 0) return false;

        int pos = s.indexOf("@");
        if (pos == -1) return false;

        pos = s.indexOf("@", pos + 1);
        if (pos != -1) return false;

        String[] parts = s.split("@");
        if (parts.length != 2) return false;
        String name   = parts[0];
        String domain = parts[1];

        if (!name.matches("[a-zA-Z0-9_.-]+")) return false;
        if (!Character.isLetter(name.charAt(0))) return false;
        if (name.charAt(name.length() - 1) == '.') return false;

        if (!domain.matches("[a-zA-Z.]+")) return false;
        if (!Character.isLetter(domain.charAt(0))) return false;
        if (domain.charAt(domain.length() - 1) == '.') return false;

        parts = domain.split("\\.");
        String tld = parts[parts.length - 1];
        if (!(tld.length() == 2 || tld.length() == 3)) return false;

        return true;
    }
}
