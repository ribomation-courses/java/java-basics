Java Basics
====

Welcome to this course.
The syllabus can be found at
[java/java-basics](https://www.ribomation.se/courses/java/java-basics.html)

Here you will find
* Installation instructions
* Solutions to the programming exercises
* Source code to demo projects


Installation Instructions
====

In order to do the programming exercises of the course, you need to have

The latest version of Java JDK installed. Currently that is `OpenJDK 13`.
* [Java JDK Download](https://adoptopenjdk.net/?variant=openjdk12&jvmVariant=hotspot)
  - N.B. This is not the ordinary Oracle download above, because starting with version 11, Oracle plan to charge for support of Java, which means you are better off going with OpenJDK in the first place. 


You also need a decent Java IDE. 
* [JetBrains IntellJ IDEA](https://www.jetbrains.com/idea/download) - This is out favourite. Go for the _Community_ version, which you can keep after the course.

In addition, you need a GIT client to easily get the solutions and demo code of the course.
* [GIT Client Download](https://git-scm.com/downloads)


Usage of this GIT Repo
====

You need to have a GIT client installed to clone this repo. 
Otherwise, you can just click on the download button and grab it all as a ZIP or TAR bundle.

Get the sources initially by a `git clone` operation. We recommend to create a top-level
directory for this course and two sub-directories; one for your own solutions and one for this GIT repo. 

Open a GIT BASH window and type
    
    mkdir -p ~/java-course/my-solutions
    cd ~/java-course
    git clone https://gitlab.com/ribomation-courses/java/java-basics.git gitlab    

Get the latest updates of this repo by a `git pull` operation

    cd ~/java-course/gitlab
    git pull


***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>

