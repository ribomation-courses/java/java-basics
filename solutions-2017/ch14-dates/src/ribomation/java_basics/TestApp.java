package ribomation.java_basics;
import java.util.Date;
import java.util.GregorianCalendar;

public class TestApp {
    public static void main(String[] args) {
        TestApp app = new TestApp();
        app.run();
    }

    void run() {
        run(new Date());
        run(new GregorianCalendar(2015,1,15).getTime());
        run(new GregorianCalendar(2016,1,15).getTime());
    }

    void run(Date in) {
        System.out.printf("IN : %1$tF %1$tT -> ", in);
        Date out = LastDayOfMonth.compute(in);
        System.out.printf("OUT: %1$tF %1$tT%n", out);
    }
}
