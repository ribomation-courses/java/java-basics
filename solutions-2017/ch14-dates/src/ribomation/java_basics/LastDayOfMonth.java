package ribomation.java_basics;

import java.util.Calendar;
import java.util.Date;

import static java.util.Calendar.*;

public class LastDayOfMonth {
    public static Date compute(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);

        c.set(HOUR_OF_DAY, 0);
        c.set(MINUTE, 0);
        c.set(SECOND, 0);
        c.set(MILLISECOND, 0);
        c.set(DAY_OF_MONTH, c.getActualMaximum(DAY_OF_MONTH));

        return c.getTime();
    }
}
