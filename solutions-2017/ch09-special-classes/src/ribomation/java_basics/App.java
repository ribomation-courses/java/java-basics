package ribomation.java_basics;

public class App {
    public static void main(String[] args) {
        App app = new App();
        app.run();
    }

    void run() {
        int lhs = 40;
        int rhs = 2;
        for (Operator op : Operator.values()) {
            System.out.printf("%d %s %d = %d%n",
                    lhs, op, rhs, Operator.eval(op, lhs, rhs));
        }
    }
}
