package ribomation.java_basics;

public enum Operator {
    add("+"), sub("-"), mul("*"), div("/");

    final String op;

    Operator(String op) {
        this.op = op;
    }

    @Override
    public String toString() {
        return op;
    }

    static int eval(Operator op, int left, int right) {
        switch (op) {
            case add:
                return left + right;
            case sub:
                return left - right;
            case mul:
                return left * right;
            case div:
                return left / right;
        }
        return 0;
    }
}
