package ribomation.java_basics;


public abstract class Shape {
    abstract float area();
}
