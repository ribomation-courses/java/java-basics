package ribomation.java_basics;
import java.util.Arrays;
import java.util.function.UnaryOperator;

public class SimpleLambdas {
    public static void main(String[] args) throws Exception {
        SimpleLambdas app = new SimpleLambdas();
        app.run();
    }

    void run() throws Exception {
        repeat(500_000, (int n) ->
                System.out.printf("[%d] Java is Cool%n", n));

        int[] input = {1, 2, 3, 4, 5};
        System.out.printf("input: %s%n", Arrays.toString(input));

        int[] output = transform(input, n -> n*n);
        System.out.printf("output: %s%n", Arrays.toString(output));
    }

    interface RepeatCallback {
        void doit(int k);
    }

    void repeat(int count, RepeatCallback expr) {
        for (int k = 1; k <= count; ++k) expr.doit(k);
    }


    int[] transform(int[] arr, UnaryOperator<Integer> func) {
        int[] result = new int[arr.length];
        for (int k = 0; k < arr.length; ++k) {
            result[k] = func.apply( arr[k] );
        }
        return result;
    }

}
