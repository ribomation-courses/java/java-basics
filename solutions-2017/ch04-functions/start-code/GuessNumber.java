package ribomation.java_basics;

import java.util.Random;
import java.util.Scanner;

public class GuessNumber {
    public static void main(String[] args) {
        GuessNumber app = new GuessNumber();
        app.init();
        app.run();
        app.print();
    }

    private Random  r;
    private Scanner input;
    private int secret, count = 1;

    private void init() {
        r      = new Random();
        input  = new Scanner(System.in);
        secret = computeSecret();
    }

    private void run() {...}

    private void print() {...}

    private void hint(int guess) {...}

    private int computeSecret() {
        return 1 + r.nextInt(10);
    }

    private int readGuess() {
        System.out.printf("Make a guess: ");
        return input.nextInt();
    }
}
