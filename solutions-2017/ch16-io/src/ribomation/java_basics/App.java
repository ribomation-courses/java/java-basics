package ribomation.java_basics;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class App {
    public static void main(String[] args) throws Exception {
        App app = new App();
        app.run();
    }

    void run() throws Exception {
        Path          csvPath   = Paths.get("DATA", "COMPANIES.csv");
        List<Company> companies = load(csvPath, ";");
        companies.forEach(System.out::println);

        Path serPath = Paths.get("DATA-GENERATED", "companies.ser");
        storeSerialized(companies, serPath);

        List<Company> companies2 = loadSerialized(serPath);
        if (companies.equals(companies2)) {
            System.out.println("Restored successfully");
        } else {
            System.out.println("Restore failed !!!");
            return;
        }

        Path csvPath2 = Paths.get("DATA-GENERATED", "companies.csv");
        storeCSV(companies2, csvPath2,
                csvHeader.replaceAll(";", "#"), "#");
    }

    String csvHeader;

    List<Company> load(Path path, String delim) throws Exception {
        BufferedReader in = Files.newBufferedReader(path);

        List<Company> companies = new ArrayList<>();
        boolean       first     = true;
        for (String csv; (csv = in.readLine()) != null; ) {
            if (first) {
                first = false;
                csvHeader = csv;
                continue;
            }
            String[] fields = csv.split(delim);
            //company;email;street;city;country
            Company company = new Company(fields[0], fields[1], fields[2], fields[3], fields[4]);
            companies.add(company);
        }
        return companies;
    }

    void storeSerialized(List<Company> companies, Path path) throws IOException {
        try (ObjectOutputStream out = new ObjectOutputStream(Files.newOutputStream(path))) {
            out.writeObject(companies);
        }
    }

    @SuppressWarnings("unchecked")
    List<Company> loadSerialized(Path path) throws IOException, ClassNotFoundException {
        try (ObjectInputStream in = new ObjectInputStream(Files.newInputStream(path))) {
            return (List<Company>) in.readObject();
        }
    }

    void storeCSV(List<Company> companies, Path path, String header, String delim) throws IOException {
        try (PrintWriter out = new PrintWriter(Files.newBufferedWriter(path))) {
            out.println(header);
            for (Company c : companies) {
                List<String> fields = Arrays.asList(
                        c.getCompanyName(), c.getEmail(),
                        c.getStreet(), c.getCity(), c.getCountry()
                );
                String csv = String.join(delim, fields);
                out.println(csv);
            }
        }
    }

}
