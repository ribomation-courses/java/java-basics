package ribomation.java_basics;

import java.io.Serializable;
import java.util.Objects;

public class Company implements Serializable {
    private String companyName, email, street, city, country;

    public Company() { }

    public Company(String companyName, String email, String street, String city, String country) {
        this.companyName = companyName;
        this.email = email;
        this.street = street;
        this.city = city;
        this.country = country;
    }

    @Override
    public String toString() {
        return "Company{" +
                "companyName='" + companyName + '\'' +
                ", email='" + email + '\'' +
                ", street='" + street + '\'' +
                ", city='" + city + '\'' +
                ", country='" + country + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Company company = (Company) o;
        return Objects.equals(companyName, company.companyName) &&
                Objects.equals(email, company.email) &&
                Objects.equals(street, company.street) &&
                Objects.equals(city, company.city) &&
                Objects.equals(country, company.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(companyName, email, street, city, country);
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
