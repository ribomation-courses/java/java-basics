package ribomation.java_basics;


import java.util.Random;

public class App {
    public static void main(String[] args) throws Exception {
        App app = new App();
        app.run();
    }

    void run() throws Exception {
        Shape[] shapes = mkShapes(10);
        print(shapes);
    }

    void print(Shape[] shapes) {
        for (Shape shape : shapes) {
            System.out.printf("area of %s is %.2f%n",
                    shape, shape.area());
        }
    }

    Shape[] mkShapes(int n) {
        Shape[] shapes = new Shape[n];
        for (int k = 0; k < shapes.length; ++k) {
            shapes[k] = mkShape();
        }
        return shapes;
    }

    Shape mkShape() {
        int w = 1 + r.nextInt(100);
        int h = 1 + r.nextInt(100);
        switch (r.nextInt(3)) {
            case 0:
                return new Rect(w, h);
            case 1:
                return new Triangle(w, h);
            case 2:
                return new Circle(w);
        }
        return new Rect(w, h);
    }

    Random r = new Random();
}
