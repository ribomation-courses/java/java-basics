package ribomation.java_basics;

public class ExceptionDemo {
    public static void main(String[] args)  {
        new ExceptionDemo().run();
    }

    void run()  {
        try {
            fun1();
        } catch (Exception e) {
            System.err.println("*** Got error");
            e.printStackTrace();
        } finally {
            System.err.println("some clean-up code");
        }
    }

    void fun1() {
        System.err.println("ExceptionDemo.fun1");
        fun2();
    }

    void fun2() {
        System.err.println("ExceptionDemo.fun2");
        fun3();
    }

    void fun3() {
        System.err.println("ExceptionDemo.fun3");
        try {
            fun4();
        } catch (Exception e) {
            throw new RuntimeException("nested error", e);
        }
    }

    void fun4() {
        System.err.println("ExceptionDemo.fun4");
        bizop();
    }

    void bizop() {
        System.err.println("ExceptionDemo.bizop");
        Integer obj = null;
        System.err.println("*** Intentional null-ptr");
        System.err.println(obj.toString());
    }

}
