package ribomation.java_basics;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class SimpleQueueTest {
    private SimpleQueue<Integer> target;

    @BeforeClass
    public static void init() {
        System.out.println("SimpleQueueTest.init");
    }

    @AfterClass
    public static void finit() {
        System.out.println("SimpleQueueTest.finit");
    }

    @Before
    public void setUp() throws Exception {
        System.out.println("SimpleQueueTest.setUp");
        target = new SimpleQueue<Integer>(3);
    }

    @After
    public void cleanup() {
        System.out.println("SimpleQueueTest.cleanup");
    }

    @Test
    public void queue_should_be_empty_after_creation() {
        System.out.println("SimpleQueueTest.queue_should_be_empty_after_creation");
        assertTrue(target.isEmpty());
    }

    @Test
    public void non_empty_queue() {
        System.out.println("SimpleQueueTest.non_empty_queue");
        target.put(42);
        assertFalse(target.isEmpty());
    }

}