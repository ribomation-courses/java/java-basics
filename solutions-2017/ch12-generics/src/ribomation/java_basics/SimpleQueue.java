package ribomation.java_basics;

public class SimpleQueue<T> {
    private final int capacity;
    private final T[] arr;
    private int getIndex=0;
    private int putIndex=0;
    private int size=0;

    public SimpleQueue(int capacity) {
        this.capacity = capacity;
        arr = (T[]) new Object[capacity];
    }

    public boolean isFull() {
        return size == capacity;
    }

    public boolean isEmpty() {
        return size==0;
    }

    public void put(T x) {
        arr[putIndex++] = x;
        if (putIndex >= capacity) putIndex = 0;
        ++size;
    }

    public T get() {
        try {
            return arr[getIndex++];
        } finally {
            --size;
            getIndex %= capacity;
        }
    }
}
