package ribomation.java_basics;


import java.util.Date;
import java.util.GregorianCalendar;

public class App {
    public static void main(String[] args) throws Exception {
        App app = new App();
        app.run();
        app.run2();
        app.run3();
    }

    void run() throws Exception {
        SimpleQueue<Integer> q = new SimpleQueue<>(5);
        for (int k = 1; !q.isFull(); ++k) {
            q.put(k);
        }
        while (!q.isEmpty()) {
            System.out.printf("got: %d%n", q.get());
        }
    }

    void run2() throws Exception {
        SimpleQueue<String> q = new SimpleQueue<>(10);
        for (int k = 1; !q.isFull(); ++k) {
            q.put("nisse-" + k);
        }
        while (!q.isEmpty()) {
            System.out.printf("got: %s%n", q.get());
        }
    }

    void run3() throws Exception {
        SimpleQueue<Date> q = new SimpleQueue<>(24);
        for (int k = 1; !q.isFull(); ++k) {
            q.put(new GregorianCalendar(2017, 11, k).getTime());
        }
        while (!q.isEmpty()) {
            System.out.printf("got: %s%n", q.get());
        }
    }

}
