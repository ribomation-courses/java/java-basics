package ribomation.java_basics;

public class Sum {
    public static void main(String[] args) {
        int N = 1000;
        int sum = N * (N + 1) / 2;
        System.out.printf("SUM(1..%d) = %d%n", N, sum);
    }
}
