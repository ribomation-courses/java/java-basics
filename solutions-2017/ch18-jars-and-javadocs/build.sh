#!/usr/bin/env bash
set -eux

BLD=./build
CLASSES=$BLD/classes
JARS=$BLD/jars
DOCS=$BLD/api
JAR=persons.jar

rm -rf $BLD
mkdir -p $CLASSES
mkdir -p $JARS
mkdir -p $DOCS

javac -d $CLASSES ./src/ribomation/java_basics/*.java
jar cvfm $JARS/$JAR ./src/META-INF/MANIFEST.MF  -C $CLASSES .
javadoc -d $DOCS -private -sourcepath ./src ribomation.java_basics
echo '---------'
tree
jar tvf $JARS/$JAR
java -jar $JARS/$JAR
