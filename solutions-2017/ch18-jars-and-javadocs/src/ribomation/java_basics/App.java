package ribomation.java_basics;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Main entry point for this application.
 */
public class App {
    public static void main(String[] args) throws Exception {
        new App().run();
    }

    /**
     * Runs the application.
     */
    void run() throws Exception {
        load(Paths.get("DATA", "persons.csv"))
                .forEach(System.out::println);
    }

    /**
     * Loads the lias of persons from the given path.
     * @param path  filename
     * @return list of persons
     */
    List<Person> load(Path path) throws IOException {
        return Files.lines(path)
                    .skip(1) //header
                    .map(Person::fromCSV)
                    .filter(Person::isFemale)
                    .filter(p -> 30 <= p.getAge() && p.getAge() <= 40)
                    .filter(p -> p.getPostCode() < 20_000)
                    .collect(Collectors.toList());
    }

}
