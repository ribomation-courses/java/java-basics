package ribomation.java_basics;

public class Person {
    private String  name;
    private boolean female;
    private int     age;
    private int     postCode;

    public static Person fromCSV(String csv) {
        return fromCSV(csv, ";");
    }

    public static Person fromCSV(String csv, String delim) {
        //name;gender;age;postCode
        String[] fields = csv.split(delim);
        return new Person(fields[0], fields[1], fields[2], fields[3]);
    }

    public Person(String name, String female, String age, String postCode) {
        this(name, female.equals("Female"),
                Integer.parseInt(age), Integer.parseInt(postCode));
    }

    public Person(String name, boolean female, int age, int postCode) {
        this.name = name;
        this.female = female;
        this.age = age;
        this.postCode = postCode;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", female=" + female +
                ", age=" + age +
                ", postCode=" + postCode +
                '}';
    }

    public String getName() {
        return name;
    }

    public boolean isFemale() {
        return female;
    }

    public int getAge() {
        return age;
    }

    public int getPostCode() {
        return postCode;
    }

}
