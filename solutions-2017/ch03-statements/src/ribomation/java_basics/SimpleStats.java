package ribomation.java_basics;

import java.util.Scanner;

public class SimpleStats {
    public static void main(String[] args) {
        final int N       = 5;
        int[]     numbers = new int[N];
        Scanner   in      = new Scanner(System.in);
        for (int k = 0; k < numbers.length; ++k) {
            System.out.print("Give a number: ");
            numbers[k] = in.nextInt();
        }

        System.out.print("Values: ");
        int sum = 0,
                prod = 1,
                min = Integer.MAX_VALUE,
                max = Integer.MIN_VALUE;
        for (int n : numbers) {
            System.out.printf("%d ", n);
            sum += n;
            prod *= n;
            if (n < min) min = n;
            max = n > max ? n : max;
        }
        System.out.println();
        System.out.printf("sum = %d%n", sum);
        System.out.printf("product = %d%n", prod);
        System.out.printf("min = %d%n", min);
        System.out.printf("max = %d%n", max);
        System.out.printf("average = %d%n", sum / numbers.length);
    }

}
