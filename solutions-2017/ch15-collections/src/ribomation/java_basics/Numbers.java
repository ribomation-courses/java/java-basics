package ribomation.java_basics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.function.Predicate;

public class Numbers {
    public static void main(String[] args) {
        Numbers app = new Numbers();
        app.run();
    }

    void run() {
        List<Integer> nums = load(10_000);
        System.out.printf("Loaded: %s%n", nums);

        Collections.sort(nums);
        System.out.printf("Sorted ASC: %s%n", nums);

        Collections.sort(nums, (lhs, rhs) ->
                Integer.compare(rhs, lhs));
        System.out.printf("Sorted DESC: %s%n", nums);

        Collections.sort(nums, (lhs, rhs) -> {
            Predicate<Integer> odd  = n -> n % 2 == 1;
            Predicate<Integer> even = n -> n % 2 == 0;
            if (even.test(lhs) && odd.test(rhs)) return -1;
            if (odd.test(lhs) && even.test(rhs)) return +1;
            return Integer.compare(lhs, rhs);
        });
        System.out.printf("Sorted Even/Odd: %s%n", nums);

        Integer min = Collections.min(nums);
        Integer max = Collections.max(nums);
        System.out.printf("Min/Max: %d / %d%n", min, max);

        long sum = nums.stream().mapToLong(n -> n).sum();
        System.out.printf("Average: %.3f%n",
                (double) sum / nums.size());
    }

    List<Integer> load(int n) {
        Random        r   = new Random();
        List<Integer> lst = new ArrayList<>();
        for (int k = 0; k < n; ++k) {
            lst.add(r.nextInt(1000) + 1);
        }
        return lst;
    }

}
