package ribomation.java_basics;
public class App {
    public static void main(String[] args) throws Exception {
        App app = new App();
        app.run();
    }

    void run() throws Exception {
        Person kalle = new Person("Kalle");
        Car    xkr   = new Car("AAA111", "XKR");
        kalle.setMyCar(xkr);
        System.out.printf("car = %s%n", xkr);
        System.out.printf("person = %s%n", kalle.toString());

        kalle = null;
        System.out.printf("person = %s%n", kalle);
        try {
            System.out.printf("person = %s%n", kalle.toString());
        } catch (Exception e) {
            System.out.println("ERR = " + e);
        }
    }
}
