package ribomation.java_basics;


public class Person {
    private String name;
    private Car myCar;

    public Person(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", myCar=" + myCar +
                '}';
    }

    public String getName() {
        return name;
    }

    public Car getMyCar() {
        return myCar;
    }

    public void setMyCar(Car myCar) {
        this.myCar = myCar;
    }

    public static void main(String[] args) {
        Person nisse = new Person("Nisse");
        System.out.println("nisse = " + nisse);

        nisse.setMyCar(new Car("XYZ987", "P18"));
        System.out.println("nisse = " + nisse);

        nisse.setMyCar(null);
        System.out.println("nisse = " + nisse);
    }
}
