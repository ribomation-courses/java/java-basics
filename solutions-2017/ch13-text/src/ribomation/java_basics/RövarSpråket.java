package ribomation.java_basics;

import java.util.regex.Pattern;

import static java.lang.Character.isAlphabetic;
import static java.lang.Character.isUpperCase;

public class RövarSpråket {
    public static String toRövare(String sentence) {
        StringBuilder buf = new StringBuilder(sentence.length() * 3);
        for (int k = 0; k < sentence.length(); ++k) {
            char ch = sentence.charAt(k);
            buf.append(ch);
            if (isConsonant(ch) && isAlphabetic(ch)) {
                buf.append(isUpperCase(ch) ? "O" : "o")
                        .append(ch);
            }
        }
        return buf.toString();
    }

    private static Pattern V = Pattern.compile("[aeiouyåäöÅÄÖ]", Pattern.CASE_INSENSITIVE);


    private static boolean isVowel(char ch) {
        return V.matcher(String.valueOf(ch)).matches();
    }

    private static boolean isConsonant(char ch) {
        return !isVowel(ch);
    }
}
