package ribomation.java_basics;

import static ribomation.java_basics.RövarSpråket.toRövare;

public class App {
    public static void main(String[] args) {
        new App().run();
        System.out.println("All tests passed !");
    }

    void run() {
        test(toRövare("Lisa"),
                "LOLisosa");

        test(toRövare("Pappa diskar 2 gånger i köket"),
                "POPapoppopa dodisoskokaror 2 gogånongogeror i kokökoketot");
    }

    void test(String target, String result) {
        if (target == null) {
            throw new RuntimeException("target is null");
        }
        if (result == null) {
            throw new RuntimeException("result is null");
        }
        if (!target.equals(result)) {
            throw new RuntimeException(String.format("'%s' != '%s'", target, result));
        }
    }
}
