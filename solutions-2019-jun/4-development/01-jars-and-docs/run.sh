#!/usr/bin/env bash
set -e

echo "--- Running plain ---"
( set -x;
    java -cp bld/classes ribomation.Hello
)

echo "--- Running cp jar ---"
( set -x;
    java -cp bld/jars/hello.jar ribomation.Hello
)

echo "--- Running exe jar ---"
( set -x;
    java -jar bld/jars/hello.jar
)
