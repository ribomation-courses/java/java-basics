#!/usr/bin/env bash
set -ex

rm -rf bld
mkdir -p bld/{classes,jars}

javac -d bld/classes $(find src/java -name '*.java')

jar cvfm bld/jars/hello.jar src/config/MANIFEST.MF -C bld/classes .
jar tvf bld/jars/hello.jar
tree
