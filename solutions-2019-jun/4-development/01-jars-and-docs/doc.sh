#!/usr/bin/env bash
set -e

mkdir -p bld/docs
javadoc -d bld/docs $(find src/java -name '*.java')
tree bld/docs
