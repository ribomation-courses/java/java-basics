package ribomation;

/**
 * Just a simple hello-world java app.
 * @author Jens Riboe
 */
public class Hello {
    /**
     * Main entry point
     * @param args cli args
     */
    public static void main(String[] args) {
        System.out.println("Hello from a Java program!");
    }
}
