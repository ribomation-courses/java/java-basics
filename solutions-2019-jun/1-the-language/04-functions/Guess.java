public class Guess {
    public static void main(String[] args) {
        var app = new Guess();
        app.init();
        app.run();
        app.print();
    }

    int secret;
    int count = 0;
    final int MAX_GUESSES = 5;

    void init() {
        var r = new java.util.Random();
        secret = 1 + r.nextInt(10);
    }

    void run() {
        do {
            var guess = readGuess();
            if (guess == secret) return;
            if (count >= MAX_GUESSES) return;
            printHint(guess);
        } while (true);
    }

    void print() {
        System.out.printf("secret=%d, # guesses=%d%n", secret, count);
    }

    void printHint(int guess) {
        if (guess < secret) 
            System.out.println("Too small");
        if (guess > secret) 
            System.out.println("Too large");
    }

    int readGuess() {
        System.out.print("Make a guess? ");
        var in = new java.util.Scanner(System. in);
        ++count;
        return in.nextInt();
    }
}
