package ribomation.java_basics;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {
    public static void main(String[] args) {
        GuessNumber app = new GuessNumber();
        app.init();
        app.run();
        app.print();
    }

    private Random  r;
    private Scanner input;
    private int secret, count = 0;

    private void init() {
        r      = new Random();
        input  = new Scanner(System.in);
        secret = 1 + r.nextInt(10);
    }

    private void run() {
		do {
            int guess = readGuess();
            ++count;
            if (guess == secret) return;
            hint(guess);
        } while (count < 10);
	}

    private void hint(int guess) {
		if (guess < secret) {
            System.out.println("Too small");
        } else if (guess > secret) {
            System.out.println("Too large");
        }
	}
	
	private void print() {
		System.out.printf("Secret = %d, number of guesses = %d%n", secret, count);
	}

    private int readGuess() {
        System.out.printf("Make a guess: ");
        return input.nextInt();
    }
}
