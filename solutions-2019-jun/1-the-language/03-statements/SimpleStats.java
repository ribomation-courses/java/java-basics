import java.util.Scanner;

public class SimpleStats {
    public static void main(String[] args) {
        var N = 5;
        var numbers = new int[N];
        var in = new Scanner(System.in);
        for (var k = 0; k < numbers.length; ++k) {
            System.out.print("Integer? ");
            numbers[k] = in.nextInt();
        }

        var sum = 0;
        var prod = 1;
        var min = Integer.MAX_VALUE;
        var max = Integer.MIN_VALUE;
        System.out.print("Numbers: ");
        for (var n : numbers) {
            System.out.printf("%d ", n);
            sum += n;
            prod *= n;
            min = n < min ? n : min;
            max = n > max ? n : max;
        }
        System.out.println();

        System.out.printf("Sum  = %d%n", sum);
        System.out.printf("Prod = %d%n", prod);
        System.out.printf("Min value = %d%n", min);
        System.out.printf("Max value = %d%n", max);
        System.out.printf("Average   = %.2f%n", (float)sum / numbers.length);
    }
}