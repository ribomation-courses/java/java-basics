package ribomation;

import java.util.Comparator;

public class DescAreaComparator implements Comparator<Shape> {
    @Override
    public int compare(Shape lhs, Shape rhs) {
        return Double.compare(rhs.area(), lhs.area());
//        var left = lhs.area();
//        var right = rhs.area();
//        if (left > right) return -1;
//        if (left < right) return +1;
//        return 0;
    }
}
