package ribomation;

import java.util.Arrays;
import java.util.Random;

public class App {
    public static void main(String[] args) {
        App app = new App();
        app.run();
    }

    void run() {
        var shapes = create(10);
        print(shapes);

        System.out.println("-".repeat(30));
        Arrays.sort(shapes, new DescAreaComparator());
        print(shapes);
    }

    void print(Shape[] shapes) {
        for (Shape s : shapes) System.out.println(s.toString());
    }

    Shape[] create(int n) {
        var shapes = new Shape[n];
        for (var k = 0; k < shapes.length; ++k) shapes[k] = create();
        return shapes;
    }

    Shape create() {
        var r = new Random();
        var type = r.nextInt(3);
        if (type == 0) return new Rect(1 + r.nextInt(10), 1 + r.nextInt(10));
        if (type == 1) return new Triangle(1 + r.nextInt(10), 1 + r.nextInt(10));
        if (type == 2) return new Circle(1 + r.nextInt(10));
        throw new RuntimeException("ooops, should not happen!");
    }

}
