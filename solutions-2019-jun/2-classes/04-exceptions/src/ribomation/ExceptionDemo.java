package ribomation;

public class ExceptionDemo {
    public static void main(String[] args) {
        var app = new ExceptionDemo();
        app.run();
    }

    void run() {
        System.out.println("ExceptionDemo.run");
        try {
            func1();
        } catch (Exception e) {
            System.out.printf("ERROR: %s%n", e);
            e.printStackTrace();
        }
        otherFunc();
        System.out.println("ExceptionDemo.run exit");
    }

    void func1() {
        System.out.println("ExceptionDemo.func1");
        func2();
        System.out.println("ExceptionDemo.func1 exit");
    }

    void func2() {
        System.out.println("ExceptionDemo.func2");
        func3();
        System.out.println("ExceptionDemo.func2 exit");
    }

    void func3() {
        System.out.println("ExceptionDemo.func3");
        func4();
        System.out.println("ExceptionDemo.func3 exit");
    }

    void func4() {
        System.out.println("ExceptionDemo.func4");
        Object ptr = null;
        System.out.println(ptr.toString());
        System.out.println("ExceptionDemo.func4 exit");
    }

    void otherFunc() {
        System.out.println("ExceptionDemo.otherFunc");
    }

}
