package ribomation;

public class Person {
    private String name;
    private Car    mycar;

    public Person(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Car getMycar() {
        return mycar;
    }

    public void setMycar(Car mycar) {
        this.mycar = mycar;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                (mycar == null ? "" : ", mycar=" + mycar) +
                '}';
    }
}
