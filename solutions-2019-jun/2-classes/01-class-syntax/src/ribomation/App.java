package ribomation;

public class App {
    public static void main(String[] args) throws InterruptedException {
        var app = new App();
        app.run();
    }

    private void run() throws InterruptedException {
        var c1 = new Car("ABC123", "Volvo Amazon");
        System.out.printf("c1: %s%n", c1);

        var p1 = new Person("Justin Time");
        System.out.printf("p1: %s%n", p1);

        p1.setMycar(c1);
        System.out.printf("p1: %s%n", p1);

        var p2 = new Person("Anna Gram");
        p2.setMycar(c1);
        System.out.printf("p2: %s%n", p2);

        p1.setMycar(null);
        System.out.printf("p1: %s%n", p1);
        System.out.printf("p2: %s%n", p2);

        p2.setMycar(null);
        System.out.printf("p2: %s%n", p2);
    }

}
