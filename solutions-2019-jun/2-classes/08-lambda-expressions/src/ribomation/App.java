package ribomation;

import java.util.Arrays;
import java.util.function.Function;

import static ribomation.Lambdas.repeat;
import static ribomation.Lambdas.transform;

public class App {
    public static void main(String[] args) {
        App app = new App();
        app.testRepeat();
//        app.testTransform();
//        app.testTransform2();
    }

    void testRepeat() {
        repeat(5, k -> System.out.printf("(%d) Java is Cool%n", k));
    }

    interface TransformFunc {
        Integer eval(Integer x);
    }

    Integer[] transform(Integer[] arr, TransformFunc func) {
        for (int k = 0; k < arr.length; ++k) {
            arr[k] = func.eval(arr[k]);
        }
        return arr;
    }

    void testTransform() {
        Integer[] numbers = {1, 2, 3, 4, 5, 6, 7, 8};
        Integer[] result  = transform(numbers, n -> n * n);
        System.out.println(Arrays.toString(result));
    }

    void testTransform2() {
        String[] txt = {"hej", "hopp", "tjabba", "tjolla hopp"};
        String[] result = Lambdas.transform(txt, String::toUpperCase);
        System.out.println(Arrays.toString(result));
    }

}
