package ribomation;

public interface Shape {
    double area();
}
