package ribomation;

public enum Operators {
    addition("+"),
    subtraction("-"),
    multiplication("*"),
    division("/"),
    modulus("%");

    final String symbol;

    Operators(String symbol) {
        this.symbol = symbol;
    }

    static int eval(Operators op, int left, int right) {
        switch (op) {
            case addition:
                return left + right;
            case subtraction:
                return left - right;
            case multiplication:
                return left * right;
            case division:
                return left / right;
            case modulus:
                return left % right;
        }
        throw new RuntimeException("Unexpected; this should not happen (WTF)");
    }
}
