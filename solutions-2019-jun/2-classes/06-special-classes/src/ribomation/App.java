package ribomation;

import static ribomation.Operators.*;

public class App {
    public static void main(String[] args) {
        var app = new App();
        app.run();
        System.out.println("-".repeat(10));
        app.run2();
    }

    private void run() {
        var a = 40;
        var b = 3;
        System.out.printf("%d + %d = %d%n", a, b, eval(addition, a, b));
        System.out.printf("%d - %d = %d%n", a, b, eval(subtraction, a, b));
        System.out.printf("%d * %d = %d%n", a, b, eval(multiplication, a, b));
        System.out.printf("%d / %d = %d%n", a, b, eval(division, a, b));
        System.out.printf("%d %% %d = %d%n", a, b, eval(modulus, a, b));
    }

    private void run2() {
        var a = 40;
        var b = 3;
        for (var op : Operators.values()) {
            System.out.printf("%d %s %d = %d%n",
                    a, op.symbol, b, eval(op, a, b));
        }
    }
}
