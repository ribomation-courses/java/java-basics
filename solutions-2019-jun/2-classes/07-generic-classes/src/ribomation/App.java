package ribomation;

import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.Date;

public class App {
    public static void main(String[] args) {
        App app = new App();
        app.run();

        final var N = 5;
        app.testIntQueue(N);
        app.testStringQueue(N);
        app.testDateQueue(N);
    }

    void run() {
        var q = new SimpleQueue<Integer>(5);
        for (var k = 1; !q.isFull(); ++k)
            q.put(k * 42);
        while (!q.isEmpty())
            System.out.println(q.get());
    }

    private <T> void drain(SimpleQueue<T> queue) {
        boolean first = true;
        while (!queue.isEmpty()) {
            System.out.printf("%s%s", (first ? "" : ", "), queue.get());
            first = false;
        }
        System.out.println();
    }

    private void testIntQueue(int n) {
        var queue = new SimpleQueue<Integer>(n);
        for (var k = 1; !queue.isFull(); ++k) {
            queue.put(k * 10);
        }
        drain(queue);
    }

    private void testStringQueue(int n) {
        var queue = new SimpleQueue<String>(n);
        for (var k = 1; !queue.isFull(); ++k) {
            queue.put("txt-" + k);
        }
        drain(queue);
    }

    private void testDateQueue(int n) {
        var queue = new SimpleQueue<Date>(n);
        LocalDate now = LocalDate.now();
        for (int k = 1; !queue.isFull(); ++k) {
            Date date = Date.from(now.plusDays(k-1).atStartOfDay().toInstant(ZoneOffset.UTC));
            queue.put(date);
        }
        drain(queue);
    }
}
