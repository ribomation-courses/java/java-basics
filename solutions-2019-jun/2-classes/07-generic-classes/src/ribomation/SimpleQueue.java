package ribomation;

public class SimpleQueue<ElemType> {
    private int        putIdx = 0;
    private int        getIdx = 0;
    private int        size   = 0;
    private ElemType[] storage;

    public SimpleQueue(int capacity) {
        storage = (ElemType[]) new Object[capacity];
    }

    void put(ElemType x) {
        storage[putIdx] = x;
        putIdx = (putIdx + 1) % capacity();
        ++size;
    }

    ElemType get() {
        ElemType x = storage[getIdx];
        getIdx = (getIdx + 1) % capacity();
        --size;
        return x;
    }

    int size() {
        return this.size;
    }

    int capacity() {
        return storage.length;
    }

    boolean isEmpty() {
        return size() == 0;
    }

    boolean isFull() {
        return size() == capacity();
    }

}
