package ribomation;

public class Triangle extends Shape {
    private int base, height;

    public Triangle(int base, int height) {
        this.base = base;
        this.height = height;
    }

    @Override
    public double area() {
        return base * height / 2.0;
    }

    @Override
    public String toString() {
        return String.format("Triangle{(%d x %d)/2 = %.2f}", base, height, area());
    }
}
