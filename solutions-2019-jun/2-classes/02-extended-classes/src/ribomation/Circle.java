package ribomation;

import java.util.Locale;

public class Circle extends Shape {
    private int radius;

    public Circle(int radius) {
        this.radius = radius;
    }

    @Override
    public double area() {
        return Math.PI * radius * radius;
    }

    @Override
    public String toString() {
        return String.format("Circle{\u03C0*%1$d*%1$d = %2$.2f}", radius, area());
    }
}
