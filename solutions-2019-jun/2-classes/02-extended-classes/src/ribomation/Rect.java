package ribomation;

public class Rect extends Shape {
    private int width, height;

    public Rect(int width, int height) {
        this.width = width;
        this.height = height;
    }

    @Override
    public double area() {
        return width * height;
    }

    @Override
    public String toString() {
        return String.format("Rect{%d x %d = %.2f}", width, height, area());
    }
}
