# Java Modules Demo

## Compile & Execute
Run the provided BASH shell script, which compiles the sources,
builds the module JAR files and executes the app.

    ./build.sh

## Create a Shrink-Wrapped JRE
Run the (2nd) provided BASH shell script, which creates a shrink-wrapped
app dedicated JRE and the runs the bundled app.

    ./create-jre.sh

