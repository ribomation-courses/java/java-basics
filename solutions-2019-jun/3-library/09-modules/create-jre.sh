#!/usr/bin/env bash
set -eux

JAR_DIR=./out/jars
JRE_DIR=./out/JRE
RUN=run-app

rm -rf $JRE_DIR

jlink --module-path $JAR_DIR \
      --add-modules ribomation.app \
      --output $JRE_DIR \
      --launcher $RUN=ribomation.app/se.ribomation.app.App \
      --strip-debug \
      --compress=2

set +x
echo '--------------------'
tree -L 1 $JRE_DIR
du -hs $JRE_DIR
du -hs $JAVA_HOME

echo '--------------------'
$JRE_DIR/bin/$RUN
echo '--------------------'
$JRE_DIR/bin/$RUN 42
