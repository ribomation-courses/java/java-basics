package ribomation2;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class App {
    public static void main(String[] args) throws Exception {
        var app = new App();
        app.run("./src/products.csv");
    }

    void run(String filename) throws IOException, ClassNotFoundException {
        var p = Path.of(filename);
        if (!Files.isReadable(p)) {
            throw new RuntimeException("cannot open file: " + filename);
        }

        var products = new ArrayList<Product>();
        var first    = true;
        for (var csv : Files.readAllLines(p)) {
            if (first) {
                first = false;
                continue;
            }

            var fields = csv.split(";");
            var name   = fields[0];
            var price  = Double.parseDouble(fields[1]);
            var count  = Integer.parseInt(fields[2]);
            products.add(new Product(name, price, count));
        }

        var tmpFile = Files.createTempFile(null, null);
        {
            var oos = new ObjectOutputStream(Files.newOutputStream(tmpFile));
            try (oos) {
                oos.writeObject(products);
                System.out.printf("written %s, file size is %d bytes%n", tmpFile, Files.size(tmpFile));
            }

            var ois = new ObjectInputStream(Files.newInputStream(tmpFile));
            try (ois) {
                var products2 = (List<Product>) ois.readObject();
                if (!products.equals(products2)) {
                    throw new RuntimeException("ooooops, not equals");
                }
            }
        }

        // GZIP compression
        {
            var gzipFile = Files.createTempFile(null, null);
            var oos = new ObjectOutputStream(new GZIPOutputStream(Files.newOutputStream(gzipFile)));
            try (oos) {
                oos.writeObject(products);
                System.out.printf("gzip file size is %d bytes%n", Files.size(gzipFile));
            }

            var ois = new ObjectInputStream(new GZIPInputStream(Files.newInputStream(gzipFile)));
            try (ois) {
                var products2 = (List<Product>) ois.readObject();
                if (!products.equals(products2)) {
                    throw new RuntimeException("ooooops, not equals");
                }
            }
        }
    }
}
