package ribomation.word_cloud;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class App {
    public static void main(String[] args) throws Exception {
        var app = new App();
        app.run("./src/musketeers.txt");
    }

    void run(String filename) throws IOException {
        var words = Pattern.compile("[^a-zA-Z]+");
        var freqs = Files.lines(Path.of(filename))
                .flatMap(words::splitAsStream)
                .filter(w -> w.length() > 3)
                .collect(Collectors.groupingBy(
                        String::toLowerCase,
                        Collectors.counting()))
                .entrySet().stream()
                .sorted((lhs, rhs) -> Long.compare(rhs.getValue(), lhs.getValue()))
                .limit(100)
                .collect(Collectors.toList());

        var maxFreq = freqs.get(0).getValue();
        var minFreq = freqs.get(freqs.size() - 1).getValue();
        var maxFont = 150;
        var minFont = 10;
        var scale   = (double) (maxFont - minFont) / (maxFreq - minFreq);
        var html = freqs.stream()
                .map(wfPair ->
                        String.format("<span style='font-size: %dpx'>%s</span>",
                                (int) (wfPair.getValue() * scale) + minFont,
                                wfPair.getKey()))
                .sorted((_lhs, _rhs) -> Math.random() < 0.5 ? -1 : +1)
                .collect(Collectors.joining("\n", "<html><body>\n", "\n</body></html>"));

        var htmlFile = Path.of("./out/word-cloud.html");
        Files.writeString(htmlFile, html);
        System.out.printf("written html file: %s%n", htmlFile);
    }
}
