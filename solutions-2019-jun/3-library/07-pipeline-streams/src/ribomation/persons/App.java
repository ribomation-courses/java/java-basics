package ribomation.persons;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

public class App {
    public static void main(String[] args) throws Exception {
        var app    = new App();
        var people = app.load("./src/persons.csv");
        people.forEach(System.out::println);
    }

    List<Person> load(String filename) throws IOException {
        return Files
                .lines(Path.of(filename))
                .skip(1)
                .map(csv -> csv.split(";"))
                .map(fields -> {
                    String  name     = fields[0];
                    boolean female   = fields[1].equals("Female");
                    int     age      = Integer.parseInt(fields[2]);
                    int     postCode = Integer.parseInt(fields[3]);
                    return new Person(name, female, age, postCode);
                })
                .filter(Person::isFemale)
                .filter(p -> 30 <= p.getAge() && p.getAge() <= 40)
                .filter(p -> p.getPostCode() < 20_000)
                .peek(System.out::println)
                .collect(Collectors.toList());
    }

}
