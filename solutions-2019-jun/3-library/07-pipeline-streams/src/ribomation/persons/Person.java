package ribomation.persons;

public class Person {
    //name;gender;age;postCode
    private String name;
    boolean female;
    private int age;
    private int postCode;

    public Person(String name, boolean female, int age, int postCode) {
        this.name = name;
        this.female = female;
        this.age = age;
        this.postCode = postCode;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", female=" + female +
                ", age=" + age +
                ", postCode=" + postCode +
                '}';
    }

    public String getName() {
        return name;
    }

    public boolean isFemale() {
        return female;
    }

    public int getAge() {
        return age;
    }

    public int getPostCode() {
        return postCode;
    }
}
