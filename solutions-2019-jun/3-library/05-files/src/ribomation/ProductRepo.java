package ribomation;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class ProductRepo {

    public List<Product> load(String filename) throws IOException {
        List<Product> result = new ArrayList<>();

        Path path = Path.of(filename);
        Files.readAllLines(path)
                .forEach(csv -> {
                    if (csv.equals("name;price;count")) return;

                    String[] fields  = csv.split(";");
                    String   name    = fields[0];
                    double   price   = Double.parseDouble(fields[1]);
                    int      count   = Integer.parseInt(fields[2]);

                    result.add(new Product(name, price, count));
                });

        return result;
    }

}
