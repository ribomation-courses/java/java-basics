package ribomation2;

public class Product {
    private String name;
    private double price;
    private int    count;

    public Product(String name, double price, int count) {
        this.name = name;
        this.price = price;
        this.count = count;
    }

    @Override
    public String toString() {
        return String.format("%s: %.2f kr, (%d)", name, price, count);
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public int getCount() {
        return count;
    }
}
