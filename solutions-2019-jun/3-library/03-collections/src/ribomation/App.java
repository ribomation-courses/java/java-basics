package ribomation;
import java.util.*;

public class App {
    public static void main(String[] args) {
        var app      = new App();
        var products = app.populate();
        products.forEach(System.out::println);

        System.out.println("-".repeat(15));
        products.sort((lhs,rhs) -> Integer.compare(rhs.getCount(), lhs.getCount()));
        products.forEach(System.out::println);

        System.out.println("-".repeat(15));
        var hashSet = new HashSet<>(products);
        hashSet.forEach(System.out::println);

        System.out.println("-".repeat(15));
        var treeSet = new TreeSet<Product>(products);
        treeSet.forEach(System.out::println);

        System.out.println("-".repeat(15));
        var treeSet2 = new TreeSet<Product>((lhs, rhs) -> Float.compare(rhs.getPrice(), lhs.getPrice()));
        treeSet2.addAll(products);
        treeSet2.forEach(System.out::println);

        System.out.println("-".repeat(15));
        var map = new TreeMap<String, Product>();
        products.forEach(p -> map.put(p.getName(), p));
        map.forEach((name, product) -> System.out.printf("%s: %s%n", name, product));

        System.out.println("-".repeat(15));
        products.removeIf(p -> p.getCount() % 2 == 0);
        products.forEach(System.out::println);
    }

    List<Product> populate() {
        var r   = new Random();
        List<Product> lst = new ArrayList<Product>();
        lst.add(new Product("apple", r.nextFloat() * 100, r.nextInt(20)));
        lst.add(new Product("banana", r.nextFloat() * 100, r.nextInt(20)));
        lst.add(new Product("coconut", r.nextFloat() * 100, r.nextInt(20)));
        lst.add(new Product("dadlar", r.nextFloat() * 100, r.nextInt(20)));
        lst.add(new Product("lemon", r.nextFloat() * 100, r.nextInt(20)));
        lst.add(new Product("orange", r.nextFloat() * 100, r.nextInt(20)));
        lst.add(new Product("pineapple", r.nextFloat() * 100, r.nextInt(20)));
        return lst;
    }

}
