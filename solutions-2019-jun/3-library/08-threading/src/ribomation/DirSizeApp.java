package ribomation;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Locale;
import java.util.concurrent.ForkJoinPool;

public class DirSizeApp {
    public static void main(String[] args) throws IOException {
        var baseDir = "C:\\Users\\jensr\\Dropbox\\Ribomation\\Ribomation-Training-2017-Autumn\\java";
        var app     = new DirSizeApp();
        app.run(baseDir);
    }

    void run(String dir) throws IOException {
        System.out.printf("Scanning dir %s...%n", dir);

        var    start  = System.nanoTime();
        Result result = new ForkJoinPool().invoke(new ComputeTask(Paths.get(dir)));
        var    end    = System.nanoTime();

        System.out.printf(Locale.ENGLISH,
                "DIR: %s%n%s%nElapsed = %.3f secs%n", dir, result, (end - start) * 1E-9);
    }
}
