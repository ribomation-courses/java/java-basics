package ribomation.topic;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class TopicService {
    private List<Topic> topics;

    {
        topics = new ArrayList<>(Arrays.asList(
                new Topic("1", "agil", "aasdfasd"),
                new Topic("2", "build-tools", "ewretwertwre"),
                new Topic("3", "cxx", "aaaaa")
        ));
    }

    public List<Topic> getAll() {
        return topics;
    }

    public Optional<Topic> findById(String id) {
        return topics.stream()
                .filter(t -> t.getId().equals(id))
                .findFirst();
    }

    public Topic insert(Topic topic) {
        topics.add(topic);
        return topic;
    }

    public void delete(String id) {
        topics.removeIf(t -> t.getId().equals(id));
    }

    public Topic replace(String id, Topic topic) {
        var target = findById(id).get();
        if (topic.getName() != null) {
            target.setName(topic.getName());
        }
        if (topic.getDescription() != null) {
            target.setDescription(topic.getDescription());
        }
        return target;
    }
}
