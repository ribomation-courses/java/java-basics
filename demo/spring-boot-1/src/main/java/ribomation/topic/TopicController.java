package ribomation.topic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/topics")
public class TopicController {
    @Autowired
    private TopicService topicSvc;

    @GetMapping
    public @ResponseBody
    ResponseEntity<List<Topic>> readAll() {
        var response = topicSvc.getAll();
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping
    public @ResponseBody()
    ResponseEntity<Topic> create(@RequestBody Topic payload) {
        var response = topicSvc.insert(payload);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public @ResponseBody
    ResponseEntity<Topic> readOne(@PathVariable String id) {
        var response = topicSvc.findById(id);
        if (response.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(response.get(), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public @ResponseBody
    ResponseEntity<Topic> update(@PathVariable String id, @RequestBody Topic payload) {
        var maybe = topicSvc.findById(id);
        if (maybe.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        Topic topic = topicSvc.replace(id, payload);
        return new ResponseEntity<>(topic, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public @ResponseBody
    ResponseEntity<Void> delete(@PathVariable String id) {
        var response = topicSvc.findById(id);
        if (response.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        topicSvc.delete(response.get().getId());
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
