package ribomation;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class App {
    public static void main(String[] args) throws Exception {
        App app = new App();
        app.run2();
    }

    void run() throws Exception {
        var db  = "javacourse";
        var url = "jdbc:mysql://localhost:3306/" + db + "?serverTimezone=UTC";
        var usr = "jens";
        var pwd = "riboe";

        Connection con  = DriverManager.getConnection(url, usr, pwd);
        Statement  stmt = con.createStatement();
        ResultSet  rs   = stmt.executeQuery("SELECT * FROM persons");
        while (rs.next()) {
            System.out.printf("%s %s (%s), %d <%s> [%s]%n",
                    rs.getString("firstName"),
                    rs.getString("lastName"),
                    rs.getString("gender"),
                    rs.getInt("age"),
                    rs.getString("email"),
                    rs.getString("url"));
        }
        rs.close();
        stmt.close();
        con.close();
    }

    void run2() throws Exception {
        var persons = findAll();
        persons.forEach(System.out::println);
    }

    List<Person> findAll() throws SQLException {
        var db  = "javacourse";
        var url = "jdbc:mysql://localhost:3306/" + db + "?serverTimezone=UTC";
        var usr = "jens";
        var pwd = "riboe";
        var sql = "SELECT * FROM persons";

        try (Connection con  = DriverManager.getConnection(url, usr, pwd)) {
            try (Statement  stmt = con.createStatement()) {
                try (ResultSet  rs   = stmt.executeQuery(sql)) {
                    return toPersons(rs);
                }
            }
        }
    }

    List<Person> toPersons(ResultSet rs) throws SQLException {
        var lst = new ArrayList<Person>();
        while (rs.next()) lst.add(toPerson(rs));
        return lst;
    }

    Person toPerson(ResultSet rs) throws SQLException {
        return new Person(
                rs.getString("firstName"),
                rs.getString("lastName"),
                rs.getString("gender"),
                rs.getInt("age"),
                rs.getString("email"),
                rs.getString("url")
        );
    }
}
