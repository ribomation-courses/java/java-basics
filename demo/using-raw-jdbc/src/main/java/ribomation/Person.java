package ribomation;

import java.util.StringJoiner;

public class Person {
    String firstName;
    String lastName;
    String gender;
    int    age;
    String email;
    String url;

    public Person() {
    }

    public Person(String firstName, String lastName, String gender, int age, String email, String url) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.age = age;
        this.email = email;
        this.url = url;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Person.class.getSimpleName() + "[", "]")
                .add("firstName='" + firstName + "'")
                .add("lastName='" + lastName + "'")
                .add("gender='" + gender + "'")
                .add("age=" + age)
                .add("email='" + email + "'")
                .add("url='" + url + "'")
                .toString();
    }
}
