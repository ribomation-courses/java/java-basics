#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "shapes/rect.h"
#include "shapes/circle.h"
#include "shapes/triangle.h"

static int uniform(int lb, int ub) {
    if (ub < lb) {
        fprintf(stderr, "uniform: invalid range [%d, %d]\n", lb, ub);
        exit(1);
    }
    int range = (ub - lb + 1);
    return lb + rand() % range;
}

static Shape* createShape() {
    switch (uniform(1, 3)) {
        case 1:
            return rect_new(uniform(1, 10), uniform(1, 10));
        case 2:
            return triangle_new(uniform(1, 10), uniform(1, 10));
        case 3:
            return circle_new(uniform(1, 10));
    }
    return 0;
}

static Shape** createShapes(unsigned n) {
    Shape** shapes = (Shape**) calloc(n, sizeof(Shape*));
    for (int k = 0; k < n; ++k) shapes[k] = createShape();
    return shapes;
}

int main(int argc, char** argv) {
    srand(time(NULL));

    const unsigned N = (argc == 1) ? 42 : atoi(argv[1]);
    Shape** shapes = createShapes(N);

    for (int k = 0; k < N; ++k) {
        printf("%2d) %s\n", k + 1, invoke_toString(shapes[k]));
    }

    for (int k = 0; k < N; ++k) free(shapes[k]);
    free(shapes);

    return 0;
}
