#pragma once

#include "shape.h"

typedef struct {
    Shape super;
    int   base;
    int   height;
} Triangle;

Shape* triangle_new(int base, int height);
