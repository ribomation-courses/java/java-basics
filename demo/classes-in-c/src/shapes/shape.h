#pragma once

typedef void* (* VirtualFunction)(void*);

typedef struct {
    VirtualFunction* vptr;
} Shape;

double invoke_area(Shape* this);
char* invoke_toString(Shape* this);

Shape* shape_init(Shape* this, VirtualFunction* vtbl);

