#pragma once

#include "shape.h"

typedef struct {
    Shape super;
    int   radius;
} Circle;

Shape* circle_new(int radius);
