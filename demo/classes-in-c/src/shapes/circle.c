#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "circle.h"

static double circle_area(Circle* this) {
    return M_PI * this->radius * this->radius;
}

static char* circle_toString(Circle* this) {
    static char buf[512];
    sprintf(buf, "Circ{PI * %d x %d = %.2f}",
            this->radius, this->radius, invoke_area((Shape*) this));
    return buf;
}

static VirtualFunction circle_vtbl[] = {
        (VirtualFunction) &circle_area,
        (VirtualFunction) &circle_toString
};

Shape* circle_new(int radius) {
    Circle* this = (Circle*) malloc(sizeof(Circle));
    shape_init((Shape*) this, circle_vtbl);

    this->radius = radius;

    return (Shape*) this;
}

