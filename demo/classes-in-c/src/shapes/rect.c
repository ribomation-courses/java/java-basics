#include <stdio.h>
#include <stdlib.h>
#include "rect.h"

static double rect_area(Rect* this) {
    return this->width * this->height;
}

static char* rect_toString(Rect* this) {
    static char buf[512];
    sprintf(buf, "Rect{%d x %d = %.2f}", this->width, this->height, invoke_area((Shape*)this));
    return buf;
}

static VirtualFunction rect_vtbl[] = {
        (VirtualFunction) &rect_area,
        (VirtualFunction) &rect_toString
};

Shape* rect_new(int width, int height) {
    Rect* this = (Rect*) malloc(sizeof(Rect));
    shape_init((Shape*) this, rect_vtbl);

    this->width = width;
    this->height = height;

    return (Shape*) this;
}

