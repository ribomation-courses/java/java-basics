#include <math.h>
#include "shape.h"

static double shape_area(Shape* this) {
    return 0;
}

static char* shape_toString(Shape* this) {
    static char buf[1];
    buf[0] = '\0';
    return buf;
}

static VirtualFunction shape_vtbl[] = {
        (VirtualFunction) &shape_area,
        (VirtualFunction) &shape_toString
};

double invoke_area(Shape* this) {
    double (* func)(Shape*);
    func = (double (*)(Shape*)) this->vptr[0];
    return (*func)(this);
}

char* invoke_toString(Shape* this) {
    char* (* func)(Shape*);
    func = (char* (*)(Shape*)) this->vptr[1];
    return (*func)(this);
}

Shape* shape_init(Shape* this, VirtualFunction* vtbl) {
    this->vptr = vtbl;
    return this;
}

