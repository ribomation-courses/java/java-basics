#include <stdio.h>
#include <stdlib.h>
#include "triangle.h"

static double triangle_area(Triangle* this) {
    return this->base * this->height / 2.0;
}

static char* triangle_toString(Triangle* this) {
    static char buf[512];
    sprintf(buf, "Triangle{(%d x %d)/2 = %.2f}",
            this->base, this->height, invoke_area((Shape*) this));
    return buf;
}

static VirtualFunction triangle_vtbl[] = {
        (VirtualFunction) &triangle_area,
        (VirtualFunction) &triangle_toString
};

Shape* triangle_new(int base, int height) {
    Triangle* this = (Triangle*) malloc(sizeof(Triangle));
    shape_init((Shape*) this, triangle_vtbl);

    this->base   = base;
    this->height = height;

    return (Shape*) this;
}
