#pragma once

#include "shape.h"

typedef struct {
    Shape super;
    int   width;
    int   height;
} Rect;

Shape* rect_new(int width, int height);
